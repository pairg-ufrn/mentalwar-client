#ifndef ABOUT_H
#define ABOUT_H

#include <windows.h>
#include <ShellApi.h>
#include "state.hpp"
#include "button.hpp"

class About : public State{
    public:
        About(Application* app);
        ~About();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

    private:
        Button backButton;

        Button UFRNButton;
        Button PAIRGButton;

        sf::Text title;
        sf::Text version;

        sf::Sprite background;
        sf::Sprite logos;
        sf::Sprite icon;
};

#endif // ABOUT_H
