#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <global.hpp>
#include <SFML/Graphics.hpp>

class Player : public sf::Drawable{
    public:
        Player();
        virtual ~Player();

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        virtual void update();

        void drawArmsAndFingers(sf::RenderTarget& target); //Necessary for passing the rope under the body

        int getForce();
        void setForce(int _force);
        void addForce(int _force);
        void applyForce(sf::Vector2f _force);

        void blink();
        void logBlink(int blinkStrength);
        int getBlinkCharges();

        float getAverageForce();
        std::vector<int> getForceValues();

        int getID();
        void setID(int _ID);

        std::string getName();
        void setName(std::string name);

        void setTeam(int _team);
        int getTeam();

        void setReady(bool _ready);
        bool isReady();

        void setPosition(sf::Vector2f newPos);
        sf::Vector2f getPosition();

        void setColor(sf::Color _color);
        sf::Color getColor();
        std::string getColorName();

        void setConnectionID(int ID);
        int getConnectionID();

        std::string getOrientation();

        void loadAvatar();

        void setHead(int _head);
        void setBody(int _body);
        void setLegs(int _legs);
        void setSex(string sex);

        void setLeft();

        void setHeadTexture(sf::Texture& texture);
        void setBodyTexture(sf::Texture& bodySprite, sf::Texture& armsSprite);
        void setLegsTexture(sf::Texture& left, sf::Texture& right);
        void setFingersTexture(sf::Texture& texture);
        void setShadowTexture(sf::Texture& texture);

        int getHead();
        int getBody();
        int getLegs();
        string getSex();

        void setExpression(EXPRESSION exp);

    protected:
        int ID;
        std::string name;
        std::vector<int> forceValues;
        int force;
        int blinkCharges;
        int team;
        int updateForce;
        int connectionID;
        bool ready;
        int head, body, legs;
        string sex;
        string orientation;

        sf::Clock updateForceClock;

        sf::Color p_color;

        sf::Sprite pointer;

        sf::Sprite headSprite;
        sf::Sprite bodySprite;
        sf::Sprite armsSprite;
        sf::Sprite fingersSprite;
        sf::Sprite leftLegSprite;
        sf::Sprite rightLegSprite;
        sf::Sprite shadowSprite;

        sf::RectangleShape p_body;

        sf::Vector2f p_position;
};

#endif // PLAYER_H
