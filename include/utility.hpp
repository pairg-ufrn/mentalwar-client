#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <global.hpp>
#include <sstream>
#include <chrono>
#include <ctime>
#include <string>
#include <istream>
#include <fstream>

using namespace std;

class Utility{
    public:
    static bool isEmpty(std::ifstream& pFile);

    static int StringToNumber ( const string &Text );

    static string toClock(double time);

    static string toString (const double& number);

    static string getDatestamp (std::string separator);

    static string getYear();
    static string getMonth();
    static string getDay();

    static string getTimestamp ();

    static string getTimestamp (std::string separator);

    static string getDatestamp (bool date, bool hour);

};

#endif // UTILITY_HPP
