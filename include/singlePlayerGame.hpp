#ifndef SINGLEPLAYERGAME_H
#define SINGLEPLAYERGAME_H

#include <game.hpp>
#include <focusMeter.hpp>
#include <player.hpp>
#include <artificialPlayer.hpp>
#include <GameDAO.hpp>

class SinglePlayerGame : public Game{
    public:
        SinglePlayerGame(int difficulty);
        ~SinglePlayerGame();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

        virtual void updateGame(const float dt);
        virtual void updateCounter(const float dt);
        virtual void updateGameover(const float dt);

    private:
        int difficulty;
        bool updateControl;

        Player human;
        ArtificialPlayer machine;

        FocusMeter humanFocusMeter;
        FocusMeter machineFocusMeter;

        virtual void testGameover();

        void updateExpressions();

        vector<sf::Sprite> blinkCharges;

        enum {MACHINE = 0, HUMAN = 1};

};

#endif // SINGLEPLAYERGAME_H
