#ifndef _APP
#define _APP

#include <stack>
#include <iostream>

#include <SFML/Graphics.hpp>

#include "VideoHandler.hpp"
#include "NetworkHandler.hpp"
#include "MindwaveModule.hpp"

#include "state.hpp"
#include "utility.hpp"
#include "global.hpp"
#include "SoundPlayer.hpp"

class State;

class Application{
    public:

    std::stack<State*> states;

    sf::RenderWindow window;

    void pushState(State* state);
    void popState();
    void changeState(State* state);
    State* peekState();

    void mainLoop();
    //void keepAlive();
    void handleEvent(sf::Event& event);
    void draw();
    //void setConnected(int con);

    void setGlobalMessage(const std::string& message);
    void takeScreenshot();
    void checkHeadsetStatus();

    Application();
    ~Application();

    VideoHandler videoHandler;

    private:

        sf::Text globalWarning;

        sf::Clock checkHeadsetConnectionClock;

        sf::Sprite headsetConnectionStatus;

        sf::Sprite headsetSignalStatus;

        sf::Sprite headsetBatteryStatus;
        sf::Clock globalWarningClock;

};
#endif // _APP
