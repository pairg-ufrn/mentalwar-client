#ifndef SOUNDSDAO_H
#define SOUNDSDAO_H

#include <iostream>
#include <map>
#include <fstream>
#include <SFML/Audio.hpp>
#include "global.hpp"

#ifndef soundsDAO
#define soundsDAO SoundsDAO::getInstance()
#endif

class SoundsDAO{
    public:
        static SoundsDAO& getInstance();
        sf::SoundBuffer& getSoundBuffer(const std::string& bufferName);

        void loadSoundBuffers();

        std::vector<std::string> getBuffersNames();

    private:
        static SoundsDAO* instance;
        SoundsDAO(SoundsDAO const&);

        std::map<std::string, sf::SoundBuffer> buffers;

        void loadSoundBuffer(const std::string& name, const std::string& filename);

        SoundsDAO(){};
        ~SoundsDAO();
};

#endif // SOUNDSDAO_H
