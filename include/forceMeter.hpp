#ifndef FORCEMETER_H
#define FORCEMETER_H

#include <SFML/Graphics.hpp>
#include "global.hpp"

class ForceMeter : public sf::Drawable{
    public:
        ForceMeter();

        int getForce();
        void setForce(const int& force);

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        void update();

        void setNumberOfPlayersTeamOne(int nPlayers);
        void setNumberOfPlayersTeamTwo(int nPlayers);

    private:
        int force = 0;
        sf::Sprite meter;
        sf::Sprite background;
        sf::Sprite overlay;

        sf::Sprite teamOneBackground;
        sf::Sprite teamOneOverlay;

        sf::Sprite teamTwoBackground;
        sf::Sprite teamTwoOverlay;

        sf::RectangleShape m_body;
        sf::Vector2f m_size;

        sf::RectangleShape meterBody;
};

#endif // FORCEMETER_H
