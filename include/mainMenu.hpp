#ifndef MAINMENU_HPP
#define MAINMENU_HPP

#include "state.hpp"
#include "button.hpp"
#include "difficultyMenu.hpp"
#include "TeamsMenu.hpp"
#include "ConfigMenu.hpp"
#include "About.hpp"

#include <SFML/Graphics.hpp>

class MainMenu : public State {
    public:
        MainMenu(Application* app);

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);
        virtual void handlePacket(packetID packet);

    private:
        sf::Text title;

        sf::Sprite background;

        Button singlePlayerButton;

        Button multiPlayerButton;

        Button optionsButton;

        Button aboutButton;

        Button exitButton;

        packetID receive;

};

#endif // MAINMENU_HPP
