#ifndef GAME_H
#define GAME_H

#include "application.hpp"
#include "forceMeter.hpp"
#include "MindwaveModule.hpp"
#include <SFML/Graphics.hpp>

class Game{
    public:
        Game();
        ~Game();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

        virtual void updateGame(const float dt);
        virtual void updateCounter(const float dt);
        virtual void updateGameover(const float dt);

        void setApp(Application* app);

        int getDuration();

    protected:
        Application* app;

        GAMESTATE GameState;

        sf::Text gameOverText;
        sf::Text counterText;
        sf::Text alertText;
        sf::Text gameClockText;

        sf::Sprite background;
        sf::Sprite noGUIbackground;
        sf::Sprite leftRope;
        sf::Sprite rightRope;
        sf::Sprite ropeStripe;
        sf::Sprite division;
        sf::Sprite gameOverBoard;
        sf::Sprite gameOverBackground;
        sf::Sprite counterSprite;

        sf::RectangleShape middleLine;

        ForceMeter forceMeter;
        bool gameover = false;
        bool GUI;
        int winner;
        sf::Clock updateClock;
        sf::Clock counterClock;
        sf::Clock gameClock;
        sf::Clock startTime;

        int att, blinkStrength, resultant;
        sf::Vector2f force;

        virtual void testGameover();

};

#endif // GAME_H
