#ifndef CONFIGMENU_H
#define CONFIGMENU_H

#include "state.hpp"
#include "button.hpp"
#include "TextBox.h"
#include "CharacterSelector.hpp"

class ConfigMenu : public State{
    public:
        ConfigMenu(Application* app);
        virtual ~ConfigMenu();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

        void saveConfigurations();

    private:

        sf::Sprite background;

        Button soundButton;
        Button saveButton;
        Button cancelButton;
        Button restoreDefaultsButton;

        TextBox playerNameTextBox;

        CharacterSelector characterSelector;

        sf::Text soundText;

        int volume;
        bool sound;

};


#endif // CONFIGMENU_H
