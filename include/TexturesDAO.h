#ifndef TEXTURESDAO_H
#define TEXTURESDAO_H

#include <iostream>
#include <map>
#include <fstream>
#include <SFML/Graphics.hpp>

#ifndef texturesDAO
#define texturesDAO TexturesDAO::getInstance()
#endif

class TexturesDAO{
    public:
        TexturesDAO();
        virtual ~TexturesDAO();

        static TexturesDAO& getInstance();
        sf::Texture& getTexture(const std::string& textureName);

        void loadTextures();

        void loadCharacters();

        void loadTexture(const std::string& name, const std::string& filename);

    private:

        static TexturesDAO* instance;

        std::map<std::string, sf::Texture> textures;
};

#endif // TEXTURESDAO_H
