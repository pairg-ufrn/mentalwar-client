#ifndef PLAYERHOLDER_H
#define PLAYERHOLDER_H

#include <SFML/Graphics.hpp>
#include <player.hpp>
#include <button.hpp>
#include <global.hpp>

class PlayerHolder : public sf::Drawable{
    public:
        PlayerHolder(Player* player, int posX, int posY);
        virtual ~PlayerHolder();

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        virtual void handleEvent(sf::Event& event);

        void setPosition(int x, int y);
        sf::Vector2f getPosition();
        Player* getPlayer();

        void setReady(bool _ready);

        sf::Sprite ready;

    private:
        Player* player;
        int slotID;

        bool isReady;

        sf::Text playerName;
        sf::RectangleShape body;

        sf::Sprite playerFace;
        sf::Sprite bodySprite;
};

#endif // PLAYERHOLDER_H
