#ifndef _GLOBAL
#define _GLOBAL

#include <clocale>

#include <iostream>
#include <sstream>

#include "configurationDAO.hpp"
#include "TexturesDAO.h"
#include "fontsDAO.hpp"
#include "soundsDAO.hpp"
#include "SoundPlayer.hpp"

#define SCREEN_LENGHT 800
#define SCREEN_HEIGHT 600

#define PLAYER_SIZE 96

#define FOCUS_METER_SIZE_X 32
#define FOCUS_METER_SIZE_Y 200

#define FORCE_METER_POS_Y 28

#define GAME_TITLE "Mental War"

#define FRAMERATE 60

#define MAX_FORCE 100

#define MIN_FORCE 0

#define LFOCUS_METER_POS_X SCREEN_LENGHT*0.02
#define RFOCUS_METER_POS_X SCREEN_LENGHT*0.94
#define FOCUS_METER_POS_Y SCREEN_HEIGHT*0.20

#define TEAM_HOLDER_SIZE_X 392
#define TEAM_HOLDER_SIZE_Y 422

const std::string GAME_VERSION = "0.1.9";

enum class GAMESTATE {COUNTDOWN = 0, PLAYING, OVER, WAIT};

enum class EXPRESSION{REGULAR = 1, ANGRY = 2, DESPERATE = 3, HAPPY = 4};

enum class packetID {None, Name, Attention, Blink, Connect, Login, LoginResponse, GameRequest, GameEnd, GameReady, GameStart, GameWin, GameLose,
                    Disconnect, Chat, Response, WrongVersion, PlayerList, ExitQueue, SpectatorChat, Broadcast, AlreadyConnected};

#endif // _GLOBAL
