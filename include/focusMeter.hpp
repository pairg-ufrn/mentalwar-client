#ifndef FOCUSMETER_H
#define FOCUSMETER_H

#include <player.hpp>
#include <global.hpp>
#include <iostream>

class FocusMeter : public sf::Drawable{
    public:
        FocusMeter();

        void addPlayer(Player* _player, bool _right = false);
        void addPlayers(vector<Player*> _players, bool _right = false);

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        void update();

        sf::Vector2f getSize();

        void setPosition(sf::Vector2f pos);

        void setRight();

        void clearAll();

    private:
        std::vector<Player*> players;
        std::vector<sf::Sprite> meters;
        std::vector<sf::Sprite> pointers;

        bool right;

        sf::Sprite meterSprite;
        sf::Sprite background;

        sf::RectangleShape m_body;
        sf::Vector2f m_size;

        sf::RectangleShape meterBody;
};

#endif // FOCUSMETER_H
