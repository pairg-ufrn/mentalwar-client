#ifndef MINDWAVEMODULE_H
#define MINDWAVEMODULE_H

#include <thinkgear.h>
#include <iostream>
using namespace std;

#ifndef mindwaveModule
#define mindwaveModule MindwaveModule::getInstance()
#endif

class MindwaveModule{
    public:
        MindwaveModule();
        virtual ~MindwaveModule();
        static MindwaveModule& getInstance();

        int getAttention();
        int getMeditation();

        int getAlpha();
        int getBeta ();
        int getGamma();
        int getDelta();
        int getTheta();

        int connect(const char* comPortName);
        void disconnect();

        int getConnectionID();

        bool isConnected();

        void poll();

        int getBatteryStrength();
        int getSignalStrength();
        bool hasPoorSignal();

        void enableBlinkDetection();

        void enableAutoReading();

        int getBlinkStrength();

    private:
        static MindwaveModule* instance;
        int connectionID;
        char* comPort;

        int lastAttentionValue;
        int lastMeditationValue;
        int lastBlinkValue;
};

#endif // MINDWAVEMODULE_H
