#ifndef SOUNDPLAYER_H
#define SOUNDPLAYER_H

#include <soundsDAO.hpp>

#ifndef soundPlayer
#define soundPlayer SoundPlayer::getInstance()
#endif

class SoundPlayer{
    public:
        SoundPlayer();
        virtual ~SoundPlayer();

        static SoundPlayer& getInstance();

        void play(const std::string& sound);
        void setVolume(const std::string& sound, const float& volume);
        void setGlobalVolume(const float& volume);
        void mute();

    private:
        static SoundPlayer* instance;

        std::map<std::string, sf::Sound> sounds;
        void initializeSounds();

};

#endif // SOUNDPLAYER_H
