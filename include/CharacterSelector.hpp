#ifndef CHARACTERSELECTOR_H
#define CHARACTERSELECTOR_H

#include <SFML/Graphics.hpp>
#include "TexturesDAO.h"
#include "button.hpp"
#include <string>

class CharacterSelector : public sf::Drawable{
    public:
        CharacterSelector();
        virtual ~CharacterSelector();

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        virtual void handleEvent(sf::Event& event);

        void setPosition(sf::Vector2f _position);
        void setSize(sf::Vector2f _size);

        void changeHead(int side);
        void changeBody(int side);
        void changeLegs(int side);

        void changeSex();

        std::string getSex();

        int getCurrentHead();
        int getCurrentBody();
        int getCurrentLegs();

    private:

        const int numberOfMaleCharacters = 4;
        const int numberOfFemaleCharacters = 2;
        int currentMaxNumber;

        std::string sex;

        sf::RectangleShape _body;
        sf::Vector2f _position;
        sf::Vector2f _size;

        sf::Sprite background;

        Button changeSexButton;

        Button changeHeadLeftButton;
        Button changeHeadRightButton;

        Button changeBodyLeftButton;
        Button changeBodyRightButton;

        Button changeLegsLeftButton;
        Button changeLegsRightButton;

        int currentHead,
            currentBody,
            currentLegs;

        sf::Sprite currentHeadSprite;
        sf::Sprite currentBodySprite;
        sf::Sprite currentArmsSprite;
        sf::Sprite currentLeftLegSprite;
        sf::Sprite currentRightLegSprite;
        sf::Sprite fingers;
        sf::Sprite shadow;

};

#endif // CHARACTERSELECTOR_H
