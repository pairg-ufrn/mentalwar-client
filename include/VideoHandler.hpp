#ifndef VIDEOHANDLER_H
#define VIDEOHANDLER_H

#include <SFML/Graphics.hpp>
#include <global.hpp>

class VideoHandler {
    public:
        VideoHandler();
        virtual ~VideoHandler();

        void fadeIn();
        void fadeOut();

        void update();

        void draw(sf::RenderWindow* window);

    private:
        sf::RectangleShape fade;
        int fadeFactor;
        bool fadein;
        bool fadeout;

};

#endif // VIDEOHANDLER_H
