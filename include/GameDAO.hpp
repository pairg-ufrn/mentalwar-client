#ifndef GAMEDAO_H
#define GAMEDAO_H

#include "player.hpp"
#include <sstream>
#include <ostream>

#ifndef gameDAO
#define gameDAO GameDAO::getInstance()
#endif

class GameDAO{
    public:
        GameDAO();
        virtual ~GameDAO();
        static GameDAO& getInstance();

        void saveGame(Player* player, int duration);

    private:
        static GameDAO* instance;
        GameDAO(GameDAO const&);
};

#endif // GAMEDAO_H
