#ifndef TEAMSMENU_HPP
#define TEAMSMENU_HPP

#include <state.hpp>
#include <playing.hpp>
#include <button.hpp>
#include <TeamHolder.hpp>
#include <MultiPlayerGame.hpp>

class TeamsMenu : public State{
    public:
        TeamsMenu(Application* app);
        virtual ~TeamsMenu();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);
        virtual void handlePacket(packetID packet);

        void insertInTeams(vector<Player*> players);

    private:

        sf::Sprite background;

        Button backButton;
        Button playButton;
        Button readyButton;

        TeamHolder teamOne;
        TeamHolder teamTwo;

        packetID receive;

        sf::Text versusText;
};

#endif // TEAMSMENU_HPP
