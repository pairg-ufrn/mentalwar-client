#ifndef TEXTBOX_H
#define TEXTBOX_H

#include <SFML/Graphics.hpp>
#include "fontsDAO.hpp"

class TextBox : public sf::Drawable{
    public:
        TextBox();
        TextBox(int posX, int posY, int sizeX, int sizeY);

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        bool clicked    (float x, float y);
        bool contains   (float x, float y);

        void setSize(float x, float y);
        void setText(const std::string& text);

        void action();

        void select();
        void deselect();

        void setTextColor(sf::Color color);
        void setShownText(std::string input);
        void setPosition(float x, float y);

        void removeWhiteSpaces();

        void processUserInput(sf::Event& event);

        sf::Vector2f getSize();
        sf::Vector2f getPosition();
        std::string getString();
        std::string getLastLetters(std::string input, int letters);

    private:

        sf::RectangleShape body;
        sf::Vector2f position;
        sf::Vector2f size;

        sf::Sprite background;

        sf::Color textColor;
        sf::Text shownText;
        std::string inputText;

        bool selected;

};

#endif // TEXTBOX_H
