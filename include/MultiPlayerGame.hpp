#ifndef MULTIPLAYERGAME_H
#define MULTIPLAYERGAME_H

#include <game.hpp>
#include <player.hpp>
#include <PlayerGroup.hpp>
#include <artificialPlayer.hpp>

class MultiPlayerGame : public Game {
    public:
        MultiPlayerGame(std::vector<Player*> firstTeam, std::vector<Player*> secondTeam);
        virtual ~MultiPlayerGame();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);
        virtual void handlePacket(packetID packet);

        virtual void updateGame(const float dt);
        virtual void updateCounter(const float dt);
        virtual void updateGameover(const float dt);

    private:
        int difficulty;
        int winner;
        int myPos;

        bool updateControl;

        PlayerGroup leftTeam;
        PlayerGroup rightTeam;

        packetID receive;

        void updateExpressions();

        virtual void testGameover();
};

#endif // MULTIPLAYERGAME_H
