#ifndef _NET
#define _NET

#ifndef network
#define network NetworkHandler::getNetwork()
#endif

#include <string>
#include "global.hpp"
#include "player.hpp"
#include "utility.hpp"
#include <SFML/Network.hpp>
#include <SFML/System.hpp>

using namespace std;
using namespace sf;


class NetworkHandler{
    private:
        //Singleton
        static NetworkHandler* instance;
        NetworkHandler();
        NetworkHandler(NetworkHandler const&);
        void operator=(NetworkHandler const&);

    public:
        static NetworkHandler& getNetwork();
        int  packet_id;

        int connected, myID, playerID;
        string myName;
        Packet packet;

        TcpSocket server;

        bool connect (string ip, int port);

        bool receiveLoginResponse();
        bool verifyVersion(string version);
        void receiveConnect();
        void receiveAverages(int& playerAverageAttention, int& teamAverageAttention);

        vector<Player*> receivePlayerList();

        void receiveAttention(int& att, int& nickname);

        void receiveMatchStatus();

        string receiveChat();

        string receiveName();

        void buildHeader (packetID pid);

        void sendLogin(string name, string version);
        void sendConnect();
        void sendReady(bool ready);
        void sendName(string myName);
        void sendChat(string msg);
        void sendAttention(int att);
        void sendBlink(int blinkStrenght);
        void sendGameRequest();
        void sendGameEnd(int winner);
        void sendGiveUp();
        void sendExitQueue();

        void sendDisconnect();
        void keepAlive();
        void handleEvents();

        packetID receivePacket();

};



#endif // _NET
