#ifndef _CONFDAO
#define _CONFDAO
#include <iostream>
#include <map>
#include <fstream>
#include "utility.hpp"
#include <stdexcept>

#ifndef configurationDAO
#define configurationDAO ConfigurationDAO::getInstance()
#endif

class ConfigurationDAO{
    public:
        static ConfigurationDAO& getInstance();

        std::string getConfiguration(std::string conf);

        void setCOMPort(int COMPort);
        void setAudio(int audio);
        void setConsole(int console);
        void setLanguage(std::string lang);
        void setUser(std::string user);
        void setSex (std::string sex);
        void setHead(int head);
        void setBody(int body);
        void setLegs(int legs);

    private:
        static ConfigurationDAO* instance;
        ConfigurationDAO(ConfigurationDAO const&);
        void operator=(ConfigurationDAO const&);

        std::map<std::string, std::string> options;

        std::string processOption(std::string opt);

        void loadConfiguration();
        void saveConfiguration();
        void parse(std::ifstream & cfgfile);

        ConfigurationDAO(){
            loadConfiguration();
        };


};

#endif // _CONFDAO
