#include "application.hpp"

void Application::pushState(State* state){
    this->states.push(state);
    return;
}

void Application::popState(){

    delete this->states.top();
    this->states.pop();

    return;
}

void Application::changeState(State* state){
    if(!this->states.empty())
        popState();
    pushState(state);

    return;
}

State* Application::peekState() {
    if(this->states.empty()) return nullptr;
    return this->states.top();
}

void Application::mainLoop() {

    sf::Event event;

    sf::Clock clock;

    sf::Time elapsed;

    float dt;

    while(this->window.isOpen()){

        elapsed = clock.restart();
        dt = elapsed.asSeconds();

        if(peekState() == nullptr) continue;

        while(window.pollEvent(event)){

            handleEvent(event);

            peekState()->handleEvent(event);

        }

        peekState()->update(dt);

        videoHandler.update();

        checkHeadsetStatus();

        this->window.clear(sf::Color::Black);

        peekState()->draw(dt);

        draw();

        this->window.display();
    }
}

void Application::draw(){
    if(globalWarningClock.getElapsedTime().asSeconds() < 4){
        window.draw(globalWarning);
    }
    window.draw(headsetConnectionStatus);
    window.draw(headsetSignalStatus);
    //videoHandler.draw(&window);

}


void Application::handleEvent(sf::Event& event){

    if(event.type == sf::Event::KeyPressed){
        switch(event.key.code){

            case sf::Keyboard::F12:
                takeScreenshot();
            break;
        }
    }

}

void Application::setGlobalMessage(const std::string& message){
    globalWarning.setString(message);
    globalWarningClock.restart();
}


void Application::takeScreenshot(){
    sf::Image screenshot = window.capture();

    std::string screenshotName = "MW_ScreenShot_" + Utility::getDatestamp("-") + "_" + Utility::getTimestamp("");

    screenshot.saveToFile("screenshots/" + screenshotName + ".png");

    setGlobalMessage("Screenshot saved!");

    cout << "ScreenShot saved!" << endl;
}

void Application::checkHeadsetStatus(){
    if(checkHeadsetConnectionClock.getElapsedTime().asSeconds() > 2){

        checkHeadsetConnectionClock.restart();

        if(!mindwaveModule.isConnected()){
            headsetConnectionStatus.setTexture(texturesDAO.getTexture("headset_disconnected"));
            headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_none"));
            //headsetBatteryStatus.setTexture(texturesDAO.getTexture("headset_battery_low"));

            mindwaveModule.connect(configurationDAO.getConfiguration("COMPort").c_str());

        }
        else{
            headsetConnectionStatus.setTexture(texturesDAO.getTexture("headset_connected"));

            int signal = mindwaveModule.getSignalStrength();
            int battery = mindwaveModule.getBatteryStrength();

            if(signal >= 180){
                headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_none"));
            }
            else if(signal >= 100){
                headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_low"));
            }
            else if(signal >= 50){
                headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_medium"));
            }
            else{
                headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_high"));
            }
        }
    }
}


Application::Application() {

    globalWarning.setFont(fontsDAO.getFont("agency_bold"));
    globalWarning.setColor(sf::Color::Red);
    globalWarning.setCharacterSize(24U);
    globalWarning.setPosition(SCREEN_LENGHT*0.4, SCREEN_HEIGHT*0.9);
    globalWarning.setString("");

    sf::Image icon;

    icon.loadFromFile("media/textures/icon.png");

    headsetConnectionStatus.setTexture(texturesDAO.getTexture("headset_disconnected"));
    headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_none"));

    headsetSignalStatus.setPosition(SCREEN_LENGHT - 36, 4);

    mindwaveModule.connect(configurationDAO.getConfiguration("COMPort").c_str());

    window.create(sf::VideoMode(SCREEN_LENGHT, SCREEN_HEIGHT), GAME_TITLE, sf::Style::Close);
    window.setIcon(271, 256, icon.getPixelsPtr());
    window.setFramerateLimit(FRAMERATE);

}

Application::~Application(){
    while(!this->states.empty()) popState();
    mindwaveModule.disconnect();
}
