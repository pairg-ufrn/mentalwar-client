#include "configurationDAO.hpp"


ConfigurationDAO& ConfigurationDAO::getInstance(){
    static ConfigurationDAO instance;
    return instance;
}

void ConfigurationDAO::loadConfiguration(){
    std::ifstream config {"mentalwar.config"};
    parse(config);
}

std::string ConfigurationDAO::getConfiguration(std::string conf){
    try{
        return options.at(conf);
    }
    catch(const std::out_of_range& err){
        cout << "ERROR: Configuration " << conf << " not found or loaded." << endl;
        std::runtime_error("ERROR: Configuration " + conf + " not found or loaded.\n");
    }
}


void ConfigurationDAO::parse(std::ifstream & cfgfile){
    std::string id, eq, val;
    while(cfgfile >> id >> eq >> val){
      if (id[0] == '#') continue;  // skip comments
      if (eq != "=") throw std::runtime_error("Parse error");
      val = processOption(val);
      options[id] = val;
    }
}

std::string ConfigurationDAO::processOption(std::string opt){
    string result = "";
    int i = 1;
    //The string must begin and end with an _"_ character
    if(opt.at(0) != '\"' or opt.at(opt.size() - 1) != '\"'){
        throw std::runtime_error("Configuration file: invalid format");
    }
    else{
        while(opt[i] != '\"'){
            result += opt[i];
            i++;
        }
    }
    return result;
}

void ConfigurationDAO::setCOMPort(int COMPort){
    options["COMPort"] = Utility::toString(COMPort);
    saveConfiguration();
}

void ConfigurationDAO::setLanguage(std::string lang){
    options["Language"] = lang;
    saveConfiguration();
}

void ConfigurationDAO::setConsole(int console){
    std::string con = Utility::toString(console);
    options["Console"] = con;
    saveConfiguration();
}

void ConfigurationDAO::setAudio(int audio){
    std::string aud = Utility::toString(audio);
    options["Audio"] = aud;
    saveConfiguration();
}

void ConfigurationDAO::setUser(std::string user){
    options["User"] = user;
    saveConfiguration();
}

void ConfigurationDAO::setSex(std::string sex){
    options["Sex"] = sex;
    saveConfiguration();
}

void ConfigurationDAO::setHead(int head){
    options["Head"] = Utility::toString(head);
    saveConfiguration();
}

void ConfigurationDAO::setBody(int body){
    options["Body"] = Utility::toString(body);
    saveConfiguration();
}

void ConfigurationDAO::setLegs(int legs){
    options["Legs"] = Utility::toString(legs);
    saveConfiguration();
}


void ConfigurationDAO::saveConfiguration(){
    ofstream output{"mentalwar.config"};
    output  << "COMPort = "     << '"' << options["COMPort"]   << '"' << "\n"
            << "Audio = "       << '"' << options["Audio"]     << '"' << "\n"
            << "Language = "    << '"' << options["Language"]  << '"' << "\n"
            << "Console = "     << '"' << options["Console"]   << '"' << "\n"
            << "Server = "      << '"' << options["Server"]    << '"' << "\n"
            << "Port = "        << '"' << options["Port"]      << '"' << "\n"
            << "User = "        << '"' << options["User"]      << '"' << "\n"
            << "Sex = "         << '"' << options["Sex"]       << '"' << "\n"
            << "Head = "        << '"' << options["Head"]      << '"' << "\n"
            << "Body = "        << '"' << options["Body"]      << '"' << "\n"
            << "Legs = "        << '"' << options["Legs"]      << '"' << "\n"
            << "GUI = "         << '"' << options["GUI"]       << '"' << "\n"
            << "ManualGame = "  << '"' << options["ManualGame"]<< '"' << "\n";
}

