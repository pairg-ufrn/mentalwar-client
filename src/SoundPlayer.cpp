#include "SoundPlayer.hpp"

SoundPlayer::SoundPlayer(){
    initializeSounds();

    if(not Utility::StringToNumber(configurationDAO.getConfiguration("Audio"))){
        setGlobalVolume(0);
    }
}

SoundPlayer::~SoundPlayer(){
    //dtor
}

SoundPlayer& SoundPlayer::getInstance(){
    static SoundPlayer instance;
    return instance;
}

void SoundPlayer::play(const std::string& sound){
    sounds.at(sound).play();
}

void SoundPlayer::setVolume(const std::string& sound, const float& volume){
    sounds.at(sound).setVolume(volume);
}


void SoundPlayer::setGlobalVolume(const float& volume){

    sf::Listener::setGlobalVolume(volume);

    for(auto& sound : sounds){
        sound.second.setVolume(volume);
    }
}

void SoundPlayer::mute(){
    setGlobalVolume(0.f);
}

void SoundPlayer::initializeSounds(){
    std::vector<std::string> soundNames = soundsDAO.getBuffersNames();
    for(auto const& name : soundNames){
        sf::Sound sound;
        sound.setBuffer(soundsDAO.getSoundBuffer(name));
        sounds[name] = sound;
    }
}
