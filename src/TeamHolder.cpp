#include "TeamHolder.hpp"

TeamHolder::TeamHolder(int posX, int posY, std::string _title){
    _body.setFillColor(sf::Color::Black);
    _body.setOutlineColor(sf::Color::White);
    _body.setOutlineThickness(-2);

    setSize(TEAM_HOLDER_SIZE_X, TEAM_HOLDER_SIZE_Y);
    setPosition(posX, posY);

    background.setTexture(texturesDAO.getTexture("team_board"));

    background.setPosition(posX + _body.getLocalBounds().width/2 - background.getTexture()->getSize().x/2,
                           posY + _body.getLocalBounds().height/2- background.getTexture()->getSize().y/2);

    title.setFont(fontsDAO.getFont("agency_bold"));
    title.setCharacterSize(20U);
    title.setColor(sf::Color::White);
    title.setString(_title);
    title.setPosition(posX + background.getTexture()->getSize().x/2 - title.getLocalBounds().width/2,
                      posY+16);

    changeTeamButton.setColor(sf::Color::Green);
    changeTeamButton.setText("+");
    changeTeamButton.setSize(32, 32);
    changeTeamButton.setPosition(posX + _body.getGlobalBounds().width*0.5 - 16, posY + _body.getGlobalBounds().height*0.95 + 24);
    changeTeamButton.disable();
}

TeamHolder::~TeamHolder(){
    //dtor
}

void TeamHolder::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    //target.draw(_body);
    target.draw(background);
    target.draw(title);
    for(auto& player : players){
        target.draw(*player);
    }
    //target.draw(changeTeamButton);
}

void TeamHolder::handleEvent(sf::Event& event){

    switch(event.type){

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            if(changeTeamButton.clicked(x, y)){

            }

            /*for(int i = 0; i < players.size(); i++){
                if(players.at(i)->removeButton.clicked(x, y)){
                    removePlayer(i);
                }
            }*/

            break;
        }
    }
}

void TeamHolder::addPlayer(Player* player){
    if(players.size() < maxPlayers){
        PlayerHolder* holder = new PlayerHolder(player,
                                                _body.getPosition().x + _body.getSize().x/2 - 113,
                                                _body.getPosition().y + 98 + 86*players.size());
        players.push_back(holder);
    }
    return;
}

void TeamHolder::removePlayer(int slotID){

    int n = players.size(); //number of players

    if(slotID < n - 1){ //If there are players after the one we are deleting...
        for(int i = slotID + 1; i < n; i++){
            sf::Vector2f pos = players.at(i)->getPosition();
            players.at(i)->setPosition(pos.x, pos.y - 73); //Set his position higher
        }
    }

    players.erase(players.begin() + slotID); //Erase the player

}

void TeamHolder::removeAllPlayers(){
    int n = players.size();
    for(int i = n - 1; i >= 0; i--){
        removePlayer(i);
    }
}


void TeamHolder::setSize(float x, float y){
    _size = sf::Vector2f(x, y);
    _body.setSize(_size);
}

void TeamHolder::setPosition(float x, float y){
    _body.setPosition(x, y);
}

std::vector<Player*> TeamHolder::getPlayers(){
    std::vector<Player*> result;

    for(auto& player : players){
        result.push_back(player->getPlayer());
    }

    return result;
}

