#include "CharacterSelector.hpp"

CharacterSelector::CharacterSelector(){

    background.setTexture(texturesDAO.getTexture("selection_board"));

    sex = configurationDAO.getConfiguration("Sex");

    if(sex == "male")
        currentMaxNumber = numberOfMaleCharacters;
    else
        currentMaxNumber = numberOfFemaleCharacters;

    currentHead = Utility::StringToNumber(configurationDAO.getConfiguration("Head"));
    currentBody = Utility::StringToNumber(configurationDAO.getConfiguration("Body"));
    currentLegs = Utility::StringToNumber(configurationDAO.getConfiguration("Legs"));

    currentHeadSprite.setTexture(texturesDAO.getTexture("character_" + sex + "_head_" + Utility::toString(currentHead) + "_1"));
    currentBodySprite.setTexture(texturesDAO.getTexture("character_" + sex + "_body_" + Utility::toString(currentBody)));
    currentArmsSprite.setTexture(texturesDAO.getTexture("character_" + sex + "_arms_" + Utility::toString(currentBody)));
    currentLeftLegSprite.setTexture(texturesDAO.getTexture("character_" + sex + "_left_leg_" + Utility::toString(currentLegs)));
    currentRightLegSprite.setTexture(texturesDAO.getTexture("character_" + sex + "_right_leg_" + Utility::toString(currentLegs)));

    shadow.setTexture(texturesDAO.getTexture("character_shadow"));
    fingers.setTexture(texturesDAO.getTexture("character_" + sex + "_fingers"));

    changeHeadLeftButton.setSize(52, 52);
    changeHeadLeftButton.setTexture(texturesDAO.getTexture("button_arrow_left"));
    changeHeadLeftButton.setHoverTexture(texturesDAO.getTexture("button_arrow_left_hover"));

    changeBodyLeftButton.setSize(52, 52);
    changeBodyLeftButton.setTexture(texturesDAO.getTexture("button_arrow_left"));
    changeBodyLeftButton.setHoverTexture(texturesDAO.getTexture("button_arrow_left_hover"));

    changeLegsLeftButton.setSize(52, 52);
    changeLegsLeftButton.setTexture(texturesDAO.getTexture("button_arrow_left"));
    changeLegsLeftButton.setHoverTexture(texturesDAO.getTexture("button_arrow_left_hover"));

    changeHeadRightButton.setSize(52, 52);
    changeHeadRightButton.setTexture(texturesDAO.getTexture("button_arrow_right"));
    changeHeadRightButton.setHoverTexture(texturesDAO.getTexture("button_arrow_right_hover"));

    changeBodyRightButton.setSize(52, 52);
    changeBodyRightButton.setTexture(texturesDAO.getTexture("button_arrow_right"));
    changeBodyRightButton.setHoverTexture(texturesDAO.getTexture("button_arrow_right_hover"));

    changeLegsRightButton.setSize(52, 52);
    changeLegsRightButton.setTexture(texturesDAO.getTexture("button_arrow_right"));
    changeLegsRightButton.setHoverTexture(texturesDAO.getTexture("button_arrow_right_hover"));

    changeSexButton.setSize(48, 48);
    changeSexButton.setText("");
    changeSexButton.setTexture(texturesDAO.getTexture("button_"+sex));
    changeSexButton.setHoverTexture(texturesDAO.getTexture("button_" + sex + "_hover"));

    setPosition(sf::Vector2f(SCREEN_LENGHT/2 - background.getTexture()->getSize().x/2, SCREEN_HEIGHT*0.1));
}

CharacterSelector::~CharacterSelector(){
    //dtor
}

void CharacterSelector::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    target.draw(background);

    target.draw(_body);
    target.draw(shadow);
    target.draw(currentRightLegSprite);
    target.draw(currentLeftLegSprite);
    target.draw(currentBodySprite);
    target.draw(currentArmsSprite);
    target.draw(currentHeadSprite);
    target.draw(fingers);

    target.draw(changeBodyLeftButton);
    target.draw(changeBodyRightButton);
    target.draw(changeHeadLeftButton);
    target.draw(changeHeadRightButton);
    target.draw(changeLegsLeftButton);
    target.draw(changeLegsRightButton);
    target.draw(changeSexButton);
}

void CharacterSelector::handleEvent(sf::Event& event){
    switch(event.type){

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            if(changeHeadLeftButton.clicked(x, y)){
                changeHead(-1);
            }

            else if(changeHeadRightButton.clicked(x, y)){
                changeHead(1);
            }

            else if(changeBodyLeftButton.clicked(x, y)){
                changeBody(-1);
            }

            else if(changeBodyRightButton.clicked(x, y)){
                changeBody(1);
            }

            else if(changeLegsLeftButton.clicked(x, y)){
                changeLegs(-1);
            }

            else if(changeLegsRightButton.clicked(x, y)){
                changeLegs(1);
            }

            else if(changeSexButton.clicked(x, y)){
                changeSex();
            }

            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            changeHeadLeftButton.hover(x, y);
            changeHeadRightButton.hover(x, y);

            changeBodyLeftButton.hover(x, y);
            changeBodyRightButton.hover(x, y);

            changeLegsLeftButton.hover(x, y);
            changeLegsRightButton.hover(x, y);

            changeSexButton.hover(x, y);

            break;
        }
    }
}


void CharacterSelector::setPosition(sf::Vector2f _position){
    this->_position = _position;
    _body.setPosition(_position);

    background.setPosition(_position);

    float lenght = background.getTexture()->getSize().x;
    float height = background.getTexture()->getSize().y;

    changeHeadLeftButton.setPosition(_position + sf::Vector2f(0, height*0.4));
    changeBodyLeftButton.setPosition(_position + sf::Vector2f(0, height*0.55));
    changeLegsLeftButton.setPosition(_position + sf::Vector2f(0, height*0.7));

    changeHeadRightButton.setPosition(_position + sf::Vector2f(lenght*0.85, height*0.4));
    changeBodyRightButton.setPosition(_position + sf::Vector2f(lenght*0.85, height*0.55));
    changeLegsRightButton.setPosition(_position + sf::Vector2f(lenght*0.85, height*0.7));

    changeSexButton.setPosition(_position + sf::Vector2f(lenght*0.5 - changeSexButton.getSize().x/2, height));

    currentHeadSprite.setPosition(_position + sf::Vector2f(lenght*0.3 + 10, height*0.3));
    currentBodySprite.setPosition(_position + sf::Vector2f(lenght*0.3 + 10, height*0.3));
    currentArmsSprite.setPosition(_position + sf::Vector2f(lenght*0.3 + 10, height*0.3));
    currentLeftLegSprite.setPosition(_position + sf::Vector2f(lenght*0.3 + 10, height*0.3));
    currentRightLegSprite.setPosition(_position + sf::Vector2f(lenght*0.3 + 10, height*0.3));
    fingers.setPosition(_position + sf::Vector2f(lenght*0.3 + 10, height*0.3));
    shadow.setPosition(_position + sf::Vector2f(lenght*0.3 + 10, height*0.3));

}


void CharacterSelector::setSize(sf::Vector2f _size){
    this->_size = _size;
    _body.setSize(_size);
}


void CharacterSelector::changeHead(int side){
    currentHead += side;
    if(currentHead <= 0) currentHead = currentMaxNumber;
    if(currentHead > currentMaxNumber) currentHead = 1;
    currentHeadSprite.setTexture(texturesDAO.getTexture("character_" + sex + "_head_" + Utility::toString(currentHead) + "_1"));
}


void CharacterSelector::changeBody(int side){
    currentBody += side;
    if(currentBody <= 0) currentBody = currentMaxNumber;
    if(currentBody > currentMaxNumber) currentBody = 1;
    currentBodySprite.setTexture(texturesDAO.getTexture("character_" + sex + "_body_" + Utility::toString(currentBody)));
    currentArmsSprite.setTexture(texturesDAO.getTexture("character_" + sex + "_arms_" + Utility::toString(currentBody)));
}


void CharacterSelector::changeLegs(int side){
    currentLegs += side;
    if(currentLegs <= 0) currentLegs = currentMaxNumber;
    if(currentLegs > currentMaxNumber) currentLegs = 1;
    currentLeftLegSprite.setTexture(texturesDAO.getTexture("character_" + sex + "_left_leg_" + Utility::toString(currentLegs)));
    currentRightLegSprite.setTexture(texturesDAO.getTexture("character_" + sex + "_right_leg_" + Utility::toString(currentLegs)));
}

void CharacterSelector::changeSex(){
    if(sex == "male"){
        sex = "female";
        currentMaxNumber = numberOfFemaleCharacters;
        changeHead(currentMaxNumber+1);
        changeBody(currentMaxNumber+1);
        changeLegs(currentMaxNumber+1);
        fingers.setTexture(texturesDAO.getTexture("character_female_fingers"));
    }
    else{
        sex = "male";
        currentMaxNumber = numberOfMaleCharacters;
        changeHead(currentMaxNumber+1);
        changeBody(currentMaxNumber+1);
        changeLegs(currentMaxNumber+1);
        fingers.setTexture(texturesDAO.getTexture("character_male_fingers"));

    }
    changeSexButton.setTexture(texturesDAO.getTexture("button_" + sex));
    changeSexButton.setHoverTexture(texturesDAO.getTexture("button_" + sex + "_hover"));
}


std::string CharacterSelector::getSex(){
    return sex;
}

int CharacterSelector::getCurrentHead(){
    return currentHead;
}

int CharacterSelector::getCurrentBody(){
    return currentBody;
}

int CharacterSelector::getCurrentLegs(){
    return currentLegs;
}

