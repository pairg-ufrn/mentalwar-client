#include "artificialPlayer.hpp"

ArtificialPlayer::ArtificialPlayer(){

}

ArtificialPlayer::~ArtificialPlayer(){
    //dtor
}

void ArtificialPlayer::update(){
    if(changeClock.getElapsedTime().asSeconds() > 2){
        srand(time(NULL));
        setForce(initialForce + rand()%10);
        changeClock.restart();
    }

    if(updateForce > force){
        force++;
    }
    else if(updateForce < force){
        force--;
    }
}

void ArtificialPlayer::setInitialForce(int _force){
    initialForce = _force;
    force = _force;
}
