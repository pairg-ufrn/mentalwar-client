#include "TeamsMenu.hpp"

TeamsMenu::TeamsMenu(Application* app) : backButton(50, 0.9*SCREEN_HEIGHT, 48, 48, ""),
                                         readyButton(0.50*SCREEN_LENGHT - 64, 0.9*SCREEN_HEIGHT, 128, 48, "Ready"),
                                         teamOne(0.01*SCREEN_LENGHT, 0.15*SCREEN_HEIGHT, /*0.35*SCREEN_LENGHT, 0.5*SCREEN_HEIGHT,*/ "Team 1"),
                                         teamTwo(0.50*SCREEN_LENGHT, 0.15*SCREEN_HEIGHT, /*0.35*SCREEN_LENGHT, 0.5*SCREEN_HEIGHT,*/ "Team 2"){
    this->app = app;

    background.setTexture(texturesDAO.getTexture("background_menu"));

    versusText.setFont(fontsDAO.getFont("agency_bold"));
    versusText.setCharacterSize(24U);
    versusText.setColor(sf::Color::White);
    versusText.setString("VS");
    versusText.setPosition(0.5*SCREEN_LENGHT - 8, 0.5*SCREEN_HEIGHT - versusText.getLocalBounds().height - 16);

    readyButton.setTexture(texturesDAO.getTexture("button_1"));
    backButton.setTexture(texturesDAO.getTexture("button_back"));

    readyButton.setHoverTexture(texturesDAO.getTexture("button_1_hover"));
    backButton.setHoverTexture(texturesDAO.getTexture("button_back_hover"));

}

TeamsMenu::~TeamsMenu(){
    //dtor
}

void TeamsMenu::draw(const float dt){
    app->window.draw(background);

    app->window.draw(backButton);
    app->window.draw(readyButton);
    app->window.draw(versusText);

    app->window.draw(teamOne);
    app->window.draw(teamTwo);
}

void TeamsMenu::update(const float dt){
    receive = network.receivePacket();

    if(receive != packetID::None){
        handlePacket(receive);
    }
}

void TeamsMenu::insertInTeams(vector<Player*> players){
    int i = 0;
    std::vector<sf::Color> colors = {sf::Color::Red, sf::Color::Cyan, sf::Color::Green, sf::Color::White, sf::Color::Blue, sf::Color::Magenta};

    for(auto& player : players){
        player->setColor(colors.at(i++));
        if(player->getTeam() == 1){
            teamOne.addPlayer(player);
        }
        else if(player->getTeam() == 2){
            teamTwo.addPlayer(player);
        }
    }
}


void TeamsMenu::handlePacket(packetID packet){
    switch(packet){
        case packetID::GameStart:{
            //start new game
            cout << "Here 1" << endl;
            app->pushState(new Playing(app, new MultiPlayerGame(teamOne.getPlayers(), teamTwo.getPlayers())));
            cout << "Here 5" << endl;
            break;
        }

        case packetID::PlayerList : {
            //update teams
            teamOne.removeAllPlayers();
            teamTwo.removeAllPlayers();

            vector<Player*> players = network.receivePlayerList();

            insertInTeams(players);
        }

        case packetID::Chat : {
            //sned message to all
        }
        break;

    }
}

void TeamsMenu::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            network.sendDisconnect();
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            if(backButton.clicked(x, y)){
                network.sendDisconnect();
                app->popState();
            }
            else if(readyButton.clicked(x, y)){
                network.sendReady(true);
            }

            teamOne.handleEvent(event);
            teamTwo.handleEvent(event);
            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            backButton.hover(x, y);
            readyButton.hover(x, y);

            break;
        }
    }
}
