#include "GameDAO.hpp"

GameDAO::GameDAO(){
    //ctor
}

GameDAO::~GameDAO(){
    //dtor
}

GameDAO& GameDAO::getInstance(){
    static GameDAO instance;
    return instance;
}


void GameDAO::saveGame(Player* player, int duration){
    std::ofstream match {"matches/" + player->getName()
                                    + "_"
                                    + Utility::getYear()
                                    + Utility::getMonth()
                                    + Utility::getDay()
                                    + "_"
                                    + Utility::getTimestamp("")
                                    + ".txt"};

    match   << "Duration: " << Utility::toString(duration) << " seconds\n"
            << "Player: " << player->getName() << "\n"
            << "Average Attention: " << player->getAverageForce() << "\n"
            << "Values:\n\n";

    for(auto& att : player->getForceValues()){
        match << att << "\n";
    }
}

GameDAO::GameDAO(GameDAO const&){

}
