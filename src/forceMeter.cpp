#include "forceMeter.hpp"

#include <iostream>

ForceMeter::ForceMeter(){

    background.setTexture(texturesDAO.getTexture("force_meter_background"));
    overlay.setTexture(texturesDAO.getTexture("force_meter"));
    teamOneBackground.setTexture(texturesDAO.getTexture("force_meter_circle"));
    teamTwoBackground.setTexture(texturesDAO.getTexture("force_meter_circle"));

    int pos_X = SCREEN_LENGHT/2 - background.getTexture()->getSize().x/2;

    background.setPosition(pos_X, FORCE_METER_POS_Y);

    teamOneBackground.setPosition(pos_X - teamOneBackground.getTexture()->getSize().x/2,
                                  background.getPosition().y - background.getTexture()->getSize().y/2 + 4);

    teamTwoBackground.setPosition(pos_X + background.getTexture()->getSize().x - teamTwoBackground.getTexture()->getSize().x/2,
                                  background.getPosition().y - background.getTexture()->getSize().y/2 + 4);

    //center origins
    overlay.setOrigin(overlay.getGlobalBounds().left + overlay.getGlobalBounds().width/2,
                      overlay.getGlobalBounds().top + overlay.getGlobalBounds().height/2);

    overlay.setPosition(pos_X + background.getLocalBounds().width/2,
                        FORCE_METER_POS_Y + background.getLocalBounds().height/2);

    sf::Vector2f newSize(overlay.getTexture()->getSize());

    m_size = newSize;

    m_body.setSize(m_size);
    m_body.setPosition(overlay.getPosition());
    m_body.setFillColor(sf::Color::White);

    meterBody.setPosition(pos_X + newSize.x/2, FORCE_METER_POS_Y);
    meterBody.setSize(sf::Vector2f(newSize.x/100.0, newSize.y));
    meterBody.setFillColor(sf::Color::Blue);

    m_body.setOrigin(m_body.getGlobalBounds().left + m_body.getGlobalBounds().width/2,
                     m_body.getGlobalBounds().top + m_body.getGlobalBounds().height/2);

    meter.setTexture(texturesDAO.getTexture("meter_arrow_vertical"));

    setForce(0);
    update();

}

int ForceMeter::getForce(){
    return force;
}

void ForceMeter::setForce(const int& force){
    this->force = force;
    update();
}

void ForceMeter::setNumberOfPlayersTeamOne(int nPlayers){
    switch(nPlayers){
        case 0:
            teamOneBackground.setTexture(texturesDAO.getTexture("icon_computer"));
        break;
        case 1:
            teamOneBackground.setTexture(texturesDAO.getTexture("icon_one_player"));
        break;
        case 2:
            teamOneBackground.setTexture(texturesDAO.getTexture("icon_two_players"));
        break;
        case 3:
            teamOneBackground.setTexture(texturesDAO.getTexture("icon_three_players"));
        break;
    }
}

void ForceMeter::setNumberOfPlayersTeamTwo(int nPlayers){
    switch(nPlayers){
        case 0:
            teamTwoBackground.setTexture(texturesDAO.getTexture("icon_computer"));
        break;
        case 1:
            teamTwoBackground.setTexture(texturesDAO.getTexture("icon_one_player"));
        break;
        case 2:
            teamTwoBackground.setTexture(texturesDAO.getTexture("icon_two_players"));
        break;
        case 3:
            teamTwoBackground.setTexture(texturesDAO.getTexture("icon_three_players"));
        break;
    }
}


void ForceMeter::update(){
    float middle = background.getPosition().x + m_size.x/2;

    float newPosition = middle + ( force * (m_size.x*0.5) ) / 100; //normalized

    meter.setPosition(newPosition, m_body.getPosition().y - m_body.getSize().y + 2);
    meterBody.setPosition(meter.getPosition());
}

void ForceMeter::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    //target.draw(m_body);
    //target.draw(meterBody);

    target.draw(background);
    target.draw(overlay);
    target.draw(teamOneBackground);
    target.draw(teamTwoBackground);
    target.draw(meter);

}
