#include "soundsDAO.hpp"


SoundsDAO& SoundsDAO::getInstance(){
    static SoundsDAO instance;
    return instance;
}

SoundsDAO::~SoundsDAO(){
    buffers.clear();
}

void SoundsDAO::loadSoundBuffers(){

    loadSoundBuffer("bip",      "bip.wav");
    loadSoundBuffer("win",      "win.wav");
    loadSoundBuffer("lose",     "lose.wav");
    loadSoundBuffer("blink",    "blink.wav");
    loadSoundBuffer("count",    "count.wav");
    loadSoundBuffer("select",   "select.wav");
    loadSoundBuffer("deselect", "deselect.wav");

    return;
}

void SoundsDAO::loadSoundBuffer(const std::string& name, const std::string& filename){

    sf::SoundBuffer buffer;
    buffer.loadFromFile("media/sounds/" + filename);

    this->buffers[name] = buffer;

    return;
}

sf::SoundBuffer& SoundsDAO::getSoundBuffer(const std::string& buffer){
    try{
        return this->buffers.at(buffer);
    }
    catch(std::out_of_range& e){
        return buffers.at("bip");
    }
}

std::vector<std::string> SoundsDAO::getBuffersNames(){
    std::vector<std::string> names;
    for(auto const& name : buffers){
        names.push_back(name.first);
    }
    return names;
}

