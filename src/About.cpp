#include "About.hpp"

About::About(Application* app) : backButton(SCREEN_LENGHT*0.05, SCREEN_HEIGHT*0.9, 48, 48, ""){

    this->app = app;

    backButton.setTexture(texturesDAO.getTexture("button_back"));
    backButton.setHoverTexture(texturesDAO.getTexture("button_back_hover"));

    logos.setTexture(texturesDAO.getTexture("about"));
    background.setTexture(texturesDAO.getTexture("background_menu"));
    icon.setTexture(texturesDAO.getTexture("icon"));

    logos.setPosition(SCREEN_LENGHT/2 - logos.getTexture()->getSize().x/2, SCREEN_HEIGHT*0.64 + logos.getTexture()->getSize().y);
    icon.setPosition (SCREEN_LENGHT/2 - icon.getTexture()->getSize().x, SCREEN_HEIGHT*0.5 - icon.getTexture()->getSize().y/2);

    UFRNButton.setSize(logos.getTexture()->getSize().x*0.4, logos.getTexture()->getSize().y);
    UFRNButton.setPosition(logos.getPosition());

    PAIRGButton.setSize(logos.getTexture()->getSize().x*0.55, logos.getTexture()->getSize().y);
    PAIRGButton.setPosition(logos.getPosition().x + logos.getTexture()->getSize().x*0.45, logos.getPosition().y);

    title.setCharacterSize(96U);
    title.setString("About");
    title.setColor(sf::Color::White);
    title.setFont(fontsDAO.getFont("life_is_goofy"));
    title.setPosition(SCREEN_LENGHT/2 - title.getLocalBounds().width/2, SCREEN_HEIGHT*0.02);

    version.setCharacterSize(32U);
    version.setString("Version 0.1.9-beta");
    version.setColor(sf::Color::White);
    version.setFont(fontsDAO.getFont("agency_bold"));
    version.setPosition(icon.getGlobalBounds().left + icon.getGlobalBounds().width + 12,
                        icon.getGlobalBounds().top + icon.getGlobalBounds().height/2 - version.getGlobalBounds().height);
}

About::~About(){
    //dtor
}

void About::draw(const float dt){
    app->window.draw(background);
    app->window.draw(title);
    app->window.draw(version);
    app->window.draw(backButton);
    app->window.draw(icon);
    app->window.draw(logos);
}

void About::update(const float dt){

}

void About::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            if(backButton.clicked(x, y)){
                app->popState();
            }

            if(UFRNButton.clicked(x, y)){
                ShellExecute(0, 0, "http://www.UFRN.br", 0, 0 , SW_SHOW );
            }

            if(PAIRGButton.clicked(x, y)){
                ShellExecute(0, 0, "http://www.PAIRG.UFRN.br", 0, 0 , SW_SHOW );
            }

            // ShellExecute(0, 0, "http://www.google.com", 0, 0 , SW_SHOW );

            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            backButton.hover(x, y);

            break;
        }


    }
}
