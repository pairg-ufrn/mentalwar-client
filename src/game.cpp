#include "game.hpp"

Game::Game(){

    GameState = GAMESTATE::COUNTDOWN;

    gameOverText.setCharacterSize(36U);
    gameOverText.setColor(sf::Color::White);
    gameOverText.setFont(fontsDAO.getFont("agency_bold"));
    gameOverText.setPosition(SCREEN_LENGHT/2, SCREEN_HEIGHT/2);
    gameOverText.setString("");

    counterText.setCharacterSize(30U);
    counterText.setColor(sf::Color::Black);
    counterText.setFont(fontsDAO.getFont("agency_bold"));
    counterText.setPosition(SCREEN_LENGHT/2 - 6, SCREEN_HEIGHT/2 - 16);
    counterText.setString("");

    alertText.setCharacterSize(30U);
    alertText.setColor(sf::Color::White);
    alertText.setFont(fontsDAO.getFont("agency_bold"));
    alertText.setPosition(SCREEN_LENGHT/2, SCREEN_HEIGHT*(4/5));
    alertText.setString("");

    gameClockText.setCharacterSize(40U);
    gameClockText.setColor(sf::Color::Black);
    gameClockText.setFont(fontsDAO.getFont("agency_bold"));
    gameClockText.setPosition(SCREEN_LENGHT/2, SCREEN_HEIGHT*(2/5));
    gameClockText.setString("");

    middleLine.setSize(sf::Vector2f(SCREEN_LENGHT/100, SCREEN_HEIGHT/5));
    middleLine.setPosition(sf::Vector2f(SCREEN_LENGHT/2 - SCREEN_LENGHT/100, SCREEN_HEIGHT/2 + SCREEN_HEIGHT/5));
    middleLine.setFillColor(sf::Color::Red);

    ropeStripe.setTexture(texturesDAO.getTexture("rope_stripe"));

    ropeStripe.setPosition(SCREEN_LENGHT/2,
                           SCREEN_HEIGHT*0.76);

    noGUIbackground.setTexture(texturesDAO.getTexture("no_GUI"));
    GUI = Utility::StringToNumber(configurationDAO.getConfiguration("GUI"));

    gameOverBoard.setTexture(texturesDAO.getTexture("gameover_board"));
    gameOverBoard.setPosition(SCREEN_LENGHT/2 - gameOverBoard.getTexture()->getSize().x/2,
                              SCREEN_HEIGHT/2 - gameOverBoard.getTexture()->getSize().y/2);
    gameOverBackground.setTexture(texturesDAO.getTexture("background_gameover"));

    counterSprite.setTexture(texturesDAO.getTexture("counter_circle"));
    counterSprite.setPosition(SCREEN_LENGHT/2 - counterSprite.getTexture()->getSize().x/2,
                              SCREEN_HEIGHT/2 - counterSprite.getTexture()->getSize().y/2);

    //blinkSound.setBuffer(soundsDAO.getSound("blink"));

   // app->videoHandler.fadeIn();
    startTime.restart();
}

Game::~Game(){
    //dtor
}

void Game::draw(const float dt){
    app->window.draw(background);
    app->window.draw(forceMeter);
}

void Game::update(const float dt){
    gameClockText.setString(Utility::toString(60 - static_cast<int>(gameClock.getElapsedTime().asSeconds())));
}

void Game::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;
    }

}

int Game::getDuration(){
    return static_cast<int>(startTime.getElapsedTime().asSeconds());
}


void Game::setApp(Application* app){
    this->app = app;
}

void Game::testGameover(){

}


void Game::updateGame(const float dt){

}

void Game::updateCounter(const float dt){

}

void Game::updateGameover(const float dt){

}

