#include "VideoHandler.hpp"

VideoHandler::VideoHandler(){
    fade.setSize(sf::Vector2f(SCREEN_LENGHT, SCREEN_HEIGHT));
    fade.setFillColor(sf::Color(255, 255, 255, 0));
    fadeFactor = 0;

    fadein = false;
    fadeout = false;
}

VideoHandler::~VideoHandler(){
    //dtor
}

void VideoHandler::fadeIn(){
    fadein = true;
    fadeout = false;
    fadeFactor = 255;
    //fade.setFillColor(sf::Color(255, 255, 255, 255));
}

void VideoHandler::fadeOut(){
    fadein = false;
    fadeout = true;

}

void VideoHandler::update(){
    if(fadein){
        if(fadeFactor >= 0){
            fadeFactor -= 5;
            fade.setFillColor(sf::Color(0, 0, 0, fadeFactor));
        }
        else{
            fadeFactor = 0;
            fadein = false;
        }
    }

    if(fadeout){
        if(fadeFactor >= 0){
            fadeFactor -= 5;
            fade.setFillColor(sf::Color(0, 0, 0, fadeFactor));
        }
        else{
            fadeFactor = 0;
            fadeout = false;
        }
    }
}


void VideoHandler::draw(sf::RenderWindow* window){

    window->draw(fade);
}
