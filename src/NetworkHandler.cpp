#include "networkHandler.hpp"
#include <iostream>

using namespace std;

NetworkHandler::NetworkHandler(){
    connected = 0;
    myName = "";
    myID = -1;
}

NetworkHandler& NetworkHandler::getNetwork(){
    static NetworkHandler instance;
    return instance;
}

bool NetworkHandler::connect (string ip, int port) {
    server.setBlocking (true);
    server.disconnect();

   // if(not connected){
        cout << "Trying to connect..." << endl;
        if (server.connect(ip, port, seconds(2)) == sf::Socket::Done) {
            cout << "Connected to server\n";
            server.setBlocking (false);
            connected = 1;
            return true;
        }
  //  }
    connected = 0;
    server.setBlocking (false);
    return false;
}

packetID NetworkHandler::receivePacket () {
    if (server.receive(packet) != Socket::Done)
        return packetID::None;

    packet >> packet_id;

    cout << "Received packet: " << static_cast<int> (packet_id) << "\n";
    return static_cast<packetID> (packet_id);
}

vector<Player*> NetworkHandler::receivePlayerList(){
    int i, listSize;
    vector<Player*> playerList;

    string nick;
    int id;
    int team; //0 to queue, 1 to team one, 2 to team two
    bool ready;
    int head, body, legs;
    string sex;

    packet >> listSize;

    for(i = 0; i < listSize; i++){
        Player* player = new Player();

        packet >> id >> nick >> team >> ready >> head >> body >> legs >> sex;

        player->setID(id);
        player->setName(nick);
        player->setTeam(team);
        player->setReady(ready);
        player->setHead(head);
        player->setBody(body);
        player->setLegs(legs);
        player->setSex(sex);

        playerList.emplace_back(player);
    }

    return playerList;
}

bool NetworkHandler::receiveLoginResponse() {
    bool correct = false;
    int id;
    string name;

    packet >> correct >> id >> name;

    if (correct){
        myID = id;
        myName = name;
    }

    return correct;
}


std::string NetworkHandler::receiveChat(){
    std::string msg;
    packet >> msg;
    return msg;
}

std::string NetworkHandler::receiveName() {
    std::string eName;
    packet >> eName;
    return eName;
}

void NetworkHandler::receiveAttention(int& att, int& ID){
    packet >> ID >> att;
    cout << "Attention received from " << ID << " [" << att << "]" << endl;

}

void NetworkHandler::receiveAverages(int& playerAverageAttention, int& teamAverageAttention){
    packet >> playerAverageAttention >> teamAverageAttention;
    cout << "Player average attention: " << playerAverageAttention << endl;
    cout << "Team average attention: " << teamAverageAttention << endl;
}

void NetworkHandler::buildHeader (packetID pid) {
    packet.clear();
    packet << static_cast<int> (pid);
}


void NetworkHandler::sendLogin(std::string name, std::string version) {
    buildHeader (packetID::Login);
    packet << name << version;

    packet << Utility::StringToNumber(configurationDAO.getConfiguration("Head"))
           << Utility::StringToNumber(configurationDAO.getConfiguration("Body"))
           << Utility::StringToNumber(configurationDAO.getConfiguration("Legs"))
           << configurationDAO.getConfiguration("Sex");

    server.send(packet);
}


void NetworkHandler::sendName (string myName) {
    buildHeader (packetID::Name);
    packet << myName;
    server.send(packet);
   // cout << "[" << myID << "] Name sended\n";
}

void NetworkHandler::sendChat(std::string msg){

    buildHeader (packetID::Chat);
    packet << msg;
    server.send(packet);
    //cout << "[" << myID << "] Chat message sended\n";
}


void NetworkHandler::sendAttention(int att){

    buildHeader (packetID::Attention);
    packet << att;

    cout << "Sended attention to server" << endl;
    server.send(packet);
}

void NetworkHandler::sendBlink(int blinkStrenght){
    buildHeader (packetID::Blink);

    packet << blinkStrenght;
    cout << "Sended blink to server" << endl;
    server.send(packet);
}


void NetworkHandler::sendReady(bool ready){
    buildHeader (packetID::GameReady);
    packet << ready;
    server.send(packet);
}


void NetworkHandler::sendGameEnd(int winner){
    buildHeader (packetID::GameEnd);
    packet << winner;
    server.send(packet);
    cout << "[" << myID << "] GameEnd sended\n";
}


void NetworkHandler::sendExitQueue(){
    buildHeader(packetID::ExitQueue);
    cout << "Sended exitQueue to server" << endl;
    server.send(packet);
}

void NetworkHandler::sendDisconnect(){
    buildHeader (packetID::Disconnect);
    server.send(packet);
    cout << "[" << myID << "] Disconnect sended\n";
}


void NetworkHandler::handleEvents(){

}
