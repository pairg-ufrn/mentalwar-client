#include "player.hpp"

Player::Player(){
    force = 60;
    name = "P";
    ready = false;
    sex = "male";
    head = 1;
    body = 1;
    legs = 1;
    orientation = "";
    blinkCharges = 3;
    p_color = sf::Color::Blue;
    p_body.setSize(sf::Vector2f(PLAYER_SIZE, PLAYER_SIZE*2));

    p_body.setOrigin(p_body.getLocalBounds().width/2, p_body.getLocalBounds().height/2);
    pointer.setOrigin(p_body.getOrigin());
    headSprite.setOrigin(p_body.getOrigin());
    bodySprite.setOrigin(p_body.getOrigin());
    armsSprite.setOrigin(p_body.getOrigin());
    fingersSprite.setOrigin(p_body.getOrigin());
    leftLegSprite.setOrigin(p_body.getOrigin());
    rightLegSprite.setOrigin(p_body.getOrigin());
    shadowSprite.setOrigin(p_body.getOrigin());

    pointer.setTexture(texturesDAO.getTexture("pointer_" + getColorName()));
    headSprite.setTexture(texturesDAO.getTexture("character_male_head_1_1"));
    bodySprite.setTexture(texturesDAO.getTexture("character_male_body_1"));
    armsSprite.setTexture(texturesDAO.getTexture("character_male_arms_1"));
    fingersSprite.setTexture(texturesDAO.getTexture("character_male_fingers"));
    leftLegSprite.setTexture(texturesDAO.getTexture("character_male_left_leg_1"));
    rightLegSprite.setTexture(texturesDAO.getTexture("character_male_right_leg_1"));
    shadowSprite.setTexture(texturesDAO.getTexture("character_shadow"));

    updateForce = 60;
}

Player::~Player(){
    //dtor
}

int Player::getForce(){
    return force;
}

float Player::getAverageForce(){
    float avg = 0.0;
    int n = 0;

    if(forceValues.size() > 0){
        for(auto& frc : forceValues){
            if(frc > 0){
                avg += frc;
                n++;
            }
        }
        avg /= n;
    }

    return avg;
}

std::vector<int> Player::getForceValues(){
    return forceValues;
}


void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    //target.draw(p_body);

    target.draw(shadowSprite);
    target.draw(rightLegSprite);
    target.draw(leftLegSprite);
    target.draw(bodySprite);
    target.draw(headSprite);

}

void Player::drawArmsAndFingers(sf::RenderTarget& target){
    target.draw(armsSprite);
    target.draw(fingersSprite);
}


void Player::setForce(int force){
    if(force <= MAX_FORCE and force >= MIN_FORCE){
        forceValues.push_back(force);
        this->updateForce = force;
    }
}

void Player::addForce(int _force){
    if(force <= MAX_FORCE and force >= MIN_FORCE){
        this->updateForce += _force;
    }
}

void Player::blink(){
    setForce(force + 10);
    blinkCharges--;
}

void Player::logBlink(int blinkStrength){
    forceValues.push_back(-1*blinkStrength);
}


int Player::getBlinkCharges(){
    return blinkCharges;
}

int Player::getID(){
    return ID;
}

void Player::setID(int _ID){
    ID = _ID;
}


std::string Player::getName(){
    return name;
}

void Player::setName(std::string name){
    this->name = name;
}
void Player::setTeam(int _team){
    team = _team;
}

int Player::getTeam(){
    return team;
}

void Player::setReady(bool _ready){
    ready = _ready;
}

bool Player::isReady(){
    return ready;
}

void Player::update(){ //do not update when forces are equal!!

    if(updateForceClock.getElapsedTime().asMilliseconds() > 150){

        updateForceClock.restart();

        if(updateForce > force){
            force++;
        }
        else if(updateForce < force){
            force--;
        }
    }
    return;
}


void Player::setPosition(sf::Vector2f newPos){

    p_position = newPos;
    p_body.setPosition(newPos);
    pointer.setPosition(newPos);
    headSprite.setPosition(newPos);
    bodySprite.setPosition(newPos);
    armsSprite.setPosition(newPos);
    fingersSprite.setPosition(newPos);
    leftLegSprite.setPosition(newPos);
    rightLegSprite.setPosition(newPos);

    shadowSprite.setPosition(newPos);
}

sf::Vector2f Player::getPosition(){
    return p_position;
}

void Player::applyForce(sf::Vector2f _force){
    setPosition(getPosition() + _force);
}

void Player::setColor(sf::Color _color){
    this->p_color = _color;
}

sf::Color Player::getColor(){
    return p_color;
}

std::string Player::getColorName(){
    if(p_color == sf::Color::White)   return "white";

    if(p_color == sf::Color::Green)   return "green";

    if(p_color == sf::Color::Magenta) return "purple";

    if(p_color == sf::Color::Red)     return "red";

    if(p_color == sf::Color::Cyan)    return "cyan";

    if(p_color == sf::Color::Blue)    return "blue";

    if(p_color == sf::Color::Black)   return "black";
}


void Player::setConnectionID(int ID){
    this->connectionID = ID;
}

int Player::getConnectionID(){
    return connectionID;
}

void Player::loadAvatar(){
    setName(configurationDAO.getConfiguration("User"));
    setSex(configurationDAO.getConfiguration("Sex"));
    setHead(Utility::StringToNumber(configurationDAO.getConfiguration("Head")));
    setBody(Utility::StringToNumber(configurationDAO.getConfiguration("Body")));
    setLegs(Utility::StringToNumber(configurationDAO.getConfiguration("Body")));
    setFingersTexture(texturesDAO.getTexture("character_" +
                                             configurationDAO.getConfiguration("Sex") +
                                             "_fingers" +
                                             orientation));
    setShadowTexture(texturesDAO.getTexture("character_shadow" + orientation));
}

void Player::setHead(int _head){
    head = _head;
    setHeadTexture(texturesDAO.getTexture("character_" + sex + "_head_" + Utility::toString(_head) + "_1" + orientation));
}

void Player::setBody(int _body){
    body = _body;
    setBodyTexture(texturesDAO.getTexture("character_" + sex + "_body_" + Utility::toString(_body) + orientation),
                   texturesDAO.getTexture("character_" + sex + "_arms_" + Utility::toString(_body) + orientation));
}

void Player::setLegs(int _legs){
    legs = _legs;
    setLegsTexture(texturesDAO.getTexture("character_" + sex + "_left_leg_" + Utility::toString(_legs) + orientation),
                   texturesDAO.getTexture("character_" + sex + "_right_leg_" + Utility::toString(_legs) + orientation));
}

void Player::setSex(string _sex){
    sex = _sex;
}

int Player::getHead(){
    return head;
}

int Player::getBody(){
    return body;
}

int Player::getLegs(){
    return legs;
}

string Player::getSex(){
    return sex;
}


void Player::setHeadTexture(sf::Texture& texture){
    headSprite.setTexture(texture);
}

void Player::setBodyTexture(sf::Texture& bodySprite, sf::Texture& armsSprite){
    this->bodySprite.setTexture(bodySprite);
    this->armsSprite.setTexture(armsSprite);
}

void Player::setLegsTexture(sf::Texture& left, sf::Texture& right){
    leftLegSprite.setTexture(left);
    rightLegSprite.setTexture(right);
}

void Player::setFingersTexture(sf::Texture& texture){
    fingersSprite.setTexture(texture);
}

void Player::setShadowTexture(sf::Texture& texture){
    shadowSprite.setTexture(texture);
}

void Player::setLeft(){
    orientation = "_left";
    setHead(head);
    setBody(body);
    setLegs(legs);
    setShadowTexture(texturesDAO.getTexture("character_shadow_left"));
    setFingersTexture(texturesDAO.getTexture("character_" + sex + "_fingers_left"));
}

std::string Player::getOrientation(){
    if(orientation == ""){
        return "right";
    }
    else{
        return "left";
    }
}


void Player::setExpression(EXPRESSION exp){
    int expInt = static_cast<int>(exp);
    setHeadTexture(texturesDAO.getTexture("character_" + sex + "_head_" + Utility::toString(head) + "_" + Utility::toString(expInt) + orientation));
}

