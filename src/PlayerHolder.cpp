#include "PlayerHolder.hpp"

PlayerHolder::PlayerHolder(Player* _player, int posX, int posY){
    player = _player;

    bodySprite.setTexture(texturesDAO.getTexture("player_holder_bar"));
    bodySprite.setPosition(posX, posY);

    body.setSize(sf::Vector2f(bodySprite.getTexture()->getSize().x, bodySprite.getTexture()->getSize().y));
    body.setFillColor(sf::Color::Black);
    body.setOutlineColor(sf::Color::White);
    body.setOutlineThickness(-1);

    playerName.setFont(fontsDAO.getFont("agency_bold"));
    playerName.setCharacterSize(20U);
    playerName.setColor(sf::Color::White);
    playerName.setString(player->getName());

    ready.setTexture(texturesDAO.getTexture("not_ready"));

    playerFace.setTexture(texturesDAO.getTexture("icon_"
                                                 + _player->getSex()
                                                 + "_" + Utility::toString(_player->getHead()) + "_"
                                                 + "left"));

    setPosition(posX, posY);

    setReady(player->isReady());
    /*removeButton.setSize(32, 32);
    removeButton.setColor(sf::Color::Red);
    removeButton.setText("-");*/

}

PlayerHolder::~PlayerHolder(){
    //dtor
}

void PlayerHolder::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    //target.draw(body);
    target.draw(bodySprite);
    target.draw(playerFace);
    target.draw(playerName);
    target.draw(ready);
}

void PlayerHolder::handleEvent(sf::Event& event){

}

void PlayerHolder::setReady(bool _ready){
    isReady = _ready;

    if(isReady){
        ready.setTexture(texturesDAO.getTexture("ready"));
    }
    else{
        ready.setTexture(texturesDAO.getTexture("not_ready"));
    }
}

void PlayerHolder::setPosition(int x, int y){
    body.setPosition(sf::Vector2f(x, y));
    playerName.setPosition(x + body.getSize().x/2 - playerName.getLocalBounds().width/2,
                           y + body.getSize().y/2 - playerName.getLocalBounds().height);

    playerFace.setPosition(x + playerFace.getTexture()->getSize().x/2 - 18,
                           y + body.getSize().y*0.5 - playerFace.getTexture()->getSize().y/2);

    ready.setPosition(x + body.getSize().x - ready.getTexture()->getSize().x - 8,
                      y + body.getSize().y*0.5 - ready.getTexture()->getSize().y/2);
}

sf::Vector2f PlayerHolder::getPosition(){
    return body.getPosition();
}


Player* PlayerHolder::getPlayer(){
    return player;
}
