#include "singlePlayerGame.hpp"

SinglePlayerGame::SinglePlayerGame(int difficulty){
    human.setPosition  (sf::Vector2f(SCREEN_LENGHT*0.30, SCREEN_HEIGHT*0.70));
    machine.setPosition(sf::Vector2f(SCREEN_LENGHT*0.70, SCREEN_HEIGHT*0.70));
    machine.setInitialForce( 50 + 10*difficulty );

    human.setLeft();
    human.loadAvatar();

    humanFocusMeter.setPosition(sf::Vector2f(LFOCUS_METER_POS_X, FOCUS_METER_POS_Y));
    machineFocusMeter.setPosition(sf::Vector2f(RFOCUS_METER_POS_X, FOCUS_METER_POS_Y));

    if(not GUI){
        soundPlayer.mute();
    }

    switch(difficulty){
        case 0:
            background.setTexture(texturesDAO.getTexture("background_plains"));
            division.setTexture(texturesDAO.getTexture("division_plains"));
        break;

        case 1:
            background.setTexture(texturesDAO.getTexture("background_desert"));
            division.setTexture(texturesDAO.getTexture("division_desert"));
        break;

        case 2:
            background.setTexture(texturesDAO.getTexture("background_volcano"));
            division.setTexture(texturesDAO.getTexture("division_volcano"));
        break;
    }

    division.setPosition(SCREEN_LENGHT/2 - division.getTexture()->getSize().x/2, SCREEN_HEIGHT*0.8);

    forceMeter.setNumberOfPlayersTeamOne(1);
    forceMeter.setNumberOfPlayersTeamTwo(0);

    humanFocusMeter.addPlayer(&human);
    machineFocusMeter.addPlayer(&machine, true);

    human.setForce(60 + 5*difficulty);

    for(int i = 0; i < human.getBlinkCharges(); i++){
        sf::Sprite charge;
        charge.setTexture(texturesDAO.getTexture("eye_opened"));
        charge.setPosition(SCREEN_LENGHT*0.25 + i*SCREEN_LENGHT*0.04,
                           SCREEN_HEIGHT*0.15);
        blinkCharges.push_back(charge);
    }

    leftRope.setTexture(texturesDAO.getTexture("left_rope_short"));
    rightRope.setTexture(texturesDAO.getTexture("right_rope_short"));

    leftRope.setPosition(SCREEN_LENGHT/2 - leftRope.getTexture()->getSize().x + 20,
                         SCREEN_HEIGHT*0.765);

    rightRope.setPosition(SCREEN_LENGHT/2 + 4,
                          SCREEN_HEIGHT*0.765);

    updateControl = true;

}

SinglePlayerGame::~SinglePlayerGame(){
    //mindwaveModule.disconnect();
}

void SinglePlayerGame::update(const float dt){

    switch (GameState){
        case GAMESTATE::COUNTDOWN:
            updateCounter(dt);
        break;

        case GAMESTATE::PLAYING:
            updateGame(dt);
        break;

        case GAMESTATE::OVER:
            updateGameover(dt);
        break;
    }

}

void SinglePlayerGame::updateGame(const float dt){

    Game::update(dt);

    if(not gameover){

        if(mindwaveModule.isConnected()){
            att = mindwaveModule.getAttention();
            blinkStrength = mindwaveModule.getBlinkStrength();
        }
        else{
            att = 0;
            blinkStrength = 0;
        }

        if(att > 0){
            human.setForce(att);
        }

        humanFocusMeter.update();
        machineFocusMeter.update();

        human.update();
        machine.update();

        if(updateClock.getElapsedTime().asSeconds() < 2 and updateControl){

             forceMeter.setForce(machine.getAverageForce() - human.getAverageForce());

            if(machine.getAverageForce() > human.getAverageForce()){
                resultant = std::min(machine.getAverageForce() - human.getAverageForce(), 20.f);
            }
            else{
                resultant = std::max(machine.getAverageForce() - human.getAverageForce(), -20.f);
            }

            force = sf::Vector2f(resultant/(SCREEN_LENGHT*0.2), 0);

            human.applyForce(force);
            machine.applyForce(force);

            leftRope.setPosition(leftRope.getPosition() + force);
            rightRope.setPosition(rightRope.getPosition() + force);
            ropeStripe.setPosition(ropeStripe.getPosition() + force);

            updateExpressions();
        }
        else{
            updateControl = false;

            if(updateClock.getElapsedTime().asSeconds() > 4){
                updateControl = true;
                updateClock.restart();
            }

        }

        if(blinkStrength > 0){
            human.logBlink(blinkStrength);
            if(blinkStrength > 150){
                if(human.getBlinkCharges() > 0){
                    human.blink();
                    soundPlayer.play("blink");
                    blinkCharges.pop_back();
                }
            }

        }
        testGameover();

    }
    else{
        std::cout << "Winner: " << winner << std::endl;
        int average = human.getAverageForce();
        if(winner){
            gameOverText.setColor(sf::Color::Green);
            gameOverText.setString("You won!\nAverage Attention: " + Utility::toString(average));
            soundPlayer.play("win");
        }
        else{
            gameOverText.setColor(sf::Color::Red);
            gameOverText.setString("You lost!\nAverage Attention: " + Utility::toString(average));
            soundPlayer.play("lose");
        }
        gameOverText.setPosition(SCREEN_LENGHT/2 - gameOverText.getLocalBounds().width/2,
                                 SCREEN_HEIGHT/2 - gameOverText.getLocalBounds().height/2);

        counterClock.restart();
        gameDAO.saveGame(&human, getDuration());
        GUI = true;
        GameState = GAMESTATE::OVER;

    }
}

void SinglePlayerGame::updateCounter(const float dt){

    int counterTime = static_cast<int>(counterClock.getElapsedTime().asSeconds());

    counterText.setString(Utility::toString(3 - counterTime));

    if(counterClock.getElapsedTime().asMilliseconds() >= 3000){
        GameState = GAMESTATE::PLAYING;
        counterText.setString("");
        soundPlayer.play("count");
    }
}

void SinglePlayerGame::updateGameover(const float dt){

    if(counterClock.getElapsedTime().asSeconds() > 5){
        app->popState();
    }

}

void SinglePlayerGame::updateExpressions(){
    if(human.getAverageForce() >= 85){
        human.setExpression(EXPRESSION::ANGRY);
    }
    else if(human.getPosition().x + PLAYER_SIZE/2 > SCREEN_LENGHT*0.45){
        human.setExpression(EXPRESSION::DESPERATE);

    }
    else{
        human.setExpression(EXPRESSION::REGULAR);
    }

    if(machine.getAverageForce() >= 75){
        machine.setExpression(EXPRESSION::ANGRY);
    }
    else if(machine.getPosition().x - PLAYER_SIZE/2 < SCREEN_LENGHT*0.55){
        machine.setExpression(EXPRESSION::DESPERATE);
    }
    else{
        machine.setExpression(EXPRESSION::REGULAR);
    }

}

void SinglePlayerGame::draw(const float dt){
    if(GUI){
        app->window.draw(background);
        app->window.draw(division);

        app->window.draw(forceMeter);
        app->window.draw(humanFocusMeter);
        app->window.draw(machineFocusMeter);

        //app->window.draw(gameClockText);

        app->window.draw(human);
        app->window.draw(machine);

        app->window.draw(leftRope);
        app->window.draw(rightRope);
        app->window.draw(ropeStripe);

        human.drawArmsAndFingers(app->window);
        machine.drawArmsAndFingers(app->window);

        for(auto& charge : blinkCharges){
            app->window.draw(charge);
        }

        //app->window.draw(middleLine);
        if(GameState == GAMESTATE::OVER){
            app->window.draw(gameOverBackground);
            app->window.draw(gameOverBoard);
            app->window.draw(gameOverText);
        }

        app->window.draw(alertText);
        if(GameState == GAMESTATE::COUNTDOWN){
            app->window.draw(counterSprite);
            app->window.draw(counterText);
        }

    }
    else{
        app->window.draw(noGUIbackground);
    }

}


void SinglePlayerGame::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;
        case sf::Event::KeyPressed:
            char input = static_cast<char>(event.text.unicode);
            switch(input){

                case sf::Keyboard::Right:
                    if(Utility::StringToNumber(configurationDAO.getConfiguration("ManualGame"))){
                        if(human.getForce() - 1 >= MIN_FORCE){
                            human.setForce(human.getForce() - 1);
                        }
                    }

                break;

                case sf::Keyboard::Left:
                    if(Utility::StringToNumber(configurationDAO.getConfiguration("ManualGame"))){
                        if(human.getForce() + 1 <= MAX_FORCE){
                            human.setForce(human.getForce() + 1);
                        }
                    }
                break;

                case sf::Keyboard::B:
                    if(Utility::StringToNumber(configurationDAO.getConfiguration("ManualGame"))){
                        if(GameState == GAMESTATE::PLAYING){
                            if(human.getBlinkCharges() > 0){
                                human.blink();
                                human.logBlink(128);
                                soundPlayer.play("blink");
                                blinkCharges.pop_back();
                            }
                        }
                    }
                break;

                case sf::Keyboard::Escape:
                    app->popState();
                break;
            }
        break;
    }

}

void SinglePlayerGame::testGameover(){

    /*if(gameClock.getElapsedTime().asSeconds() >= 60){
        gameover = true;
        if(SCREEN_LENGHT/2 - (human.getPosition().x + PLAYER_SIZE/2) > machine.getPosition().x - PLAYER_SIZE/2 - SCREEN_LENGHT/2 ){
            winner = HUMAN;
        }
        else{
            winner = MACHINE;
        }
    }
    else{*/
        if(human.getPosition().x + PLAYER_SIZE/2 > SCREEN_LENGHT/2){
            winner = MACHINE;
            gameover = true;
        }

        if(machine.getPosition().x - PLAYER_SIZE/2 < SCREEN_LENGHT/2){
            winner = HUMAN;
            gameover = true;
        }
   // }

}

