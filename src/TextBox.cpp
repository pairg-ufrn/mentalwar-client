#include "TextBox.h"

TextBox::TextBox(){
    //ctor

    selected = false;

    shownText.setFont(fontsDAO.getFont("agency_bold"));
    shownText.setString("");
    shownText.setCharacterSize(25U);
    shownText.setColor(sf::Color::Black);

    body.setFillColor(sf::Color::White);

}

TextBox::TextBox(int posX, int posY, int sizeX, int sizeY){
    setSize(sizeX, sizeY);

    /*this->title.setFont(fontsDAO.getFont("agencyBold"));
    this->title.setPosition(text_x, text_y);
    this->title.setString(desc);
    this->title.setCharacterSize(20U);*/

    selected = false;

    shownText.setFont(fontsDAO.getFont("agency_bold"));
    shownText.setPosition(posX + 4, posY + 3);
    shownText.setString("");
    shownText.setCharacterSize(22U);
    shownText.setColor(sf::Color::Black);

    body.setFillColor(sf::Color::White);

    /*if(size == 0)
        background.setTexture(*Textures::inputBox);
    else if(size == 2)
        background.setTexture(*Textures::largeInputBox);*/

    background.setPosition(posX, posY);

    setPosition(posX, posY);
}


void TextBox::processUserInput(sf::Event& event){
    char letter;

    switch(event.type){

        case sf::Event::TextEntered:

            letter = static_cast<char>(event.text.unicode);

            if(letter != '\b'){
                if(inputText.size() < 18){
                    inputText.push_back(letter);
                    setShownText(inputText);
                }
            }
            else if(inputText.size() > 0){
                inputText.pop_back();
                setShownText(inputText);
            }

        break;
    }
}

void TextBox::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    target.draw(background);

    //target.draw(body);

    target.draw(shownText);
}


void TextBox::action(){

}

void TextBox::select(){
    selected = true;
    //shownText.setString(shownText.getString() + "|");
}

void TextBox::deselect(){

    std::string newString = shownText.getString();

    if(newString.size() > 0 && selected){
        newString.pop_back();
        selected = false;
    }

    shownText.setString(newString);
}

void TextBox::setTextColor(sf::Color color){
    textColor = color;
    shownText.setColor(color);
}


void TextBox::setShownText(std::string input){
    inputText = input;

    shownText.setString("");

    int numberOfLetters = 0;
    int i = input.size() - 1;

    std::string aux = "";

    if(i < 0) return;

    //The size of the text plus a constant (to keep a margin) is smaller than the size of the box
    while(shownText.getGlobalBounds().width + 16 < getSize().x && i >= 0){

       //Add the letters of the input backwards
        aux += input.at(i--);

        shownText.setString(aux);

        numberOfLetters++;

    }

    shownText.setString(getLastLetters(input, numberOfLetters) + "|");
}

void TextBox::setPosition(float x, float y){
    sf::Vector2f newPosition(x, y);

    position = newPosition;

    body.setPosition(newPosition);

    background.setPosition(newPosition);

    shownText.setPosition(x + 4, y + 3);
}

sf::Vector2f TextBox::getSize(){
    return size;
}

sf::Vector2f TextBox::getPosition(){
    return position;
}

std::string TextBox::getString(){
    return inputText;
}

void TextBox::setText(const std::string& text){
    inputText = text;
    setShownText(inputText);
}



std::string TextBox::getLastLetters(std::string input, int letters){
    int i;
    std::string output = "";

    if(input.size() > letters){
        for(i = input.size() - letters; i < input.size(); i++){
            output += input[i];
        }
        return output;
    }
    else return input;
}

bool TextBox::clicked(float x, float y){
    sf::FloatRect body(getPosition().x, getPosition().y, size.x, size.y);
    if(body.contains(x, y)){
        return true;
    }
    else{
        return false;
    }
}

bool TextBox::contains(float x, float y){
    if(contains(x, y) and sf::Mouse::isButtonPressed(sf::Mouse::Left)){
        return true;
    }
    else{
        return false;
    }
}

void TextBox::removeWhiteSpaces(){
    for(int i = 0; i < inputText.size(); i++){
        if(inputText.at(i) == ' '){
            inputText.erase(inputText.begin() + i);
        }
    }
}


void TextBox::setSize(float x, float y){
    sf::Vector2f newSize(x, y);
    size = newSize;
    body.setSize(size);
}
