#include "mainMenu.hpp"

MainMenu::MainMenu(Application* app) :  singlePlayerButton  (336, SCREEN_HEIGHT*0.4, 148, 56, "Single Player"),
                                        multiPlayerButton   (336, SCREEN_HEIGHT*0.52, 148, 56, "Multi Player"),
                                        optionsButton       (SCREEN_LENGHT*0.88, SCREEN_HEIGHT*0.85, 64, 64, ""),
                                        aboutButton         (SCREEN_LENGHT*0.88, SCREEN_HEIGHT*0.72, 128, 48, ""),
                                        exitButton          (SCREEN_LENGHT*0.04, SCREEN_HEIGHT*0.85, 64, 64, ""){
    this->app = app;

    background.setTexture(texturesDAO.getTexture("background"));

    title.setCharacterSize(96U);
    title.setString("Mental War");
    title.setColor(sf::Color::White);
    title.setFont(fontsDAO.getFont("moustache_club"));
    title.setPosition(SCREEN_LENGHT/2 - title.getLocalBounds().width/2, SCREEN_HEIGHT*0.05);

    singlePlayerButton.setTexture(texturesDAO.getTexture("button_2"));
    multiPlayerButton.setTexture(texturesDAO.getTexture("button_2"));
    optionsButton.setTexture(texturesDAO.getTexture("button_gear"));
    aboutButton.setTexture(texturesDAO.getTexture("button_info"));
    exitButton.setTexture(texturesDAO.getTexture("button_onoff"));

    singlePlayerButton.setHoverTexture(texturesDAO.getTexture("button_2_hover"));
    multiPlayerButton.setHoverTexture(texturesDAO.getTexture("button_2_hover"));
    optionsButton.setHoverTexture(texturesDAO.getTexture("button_gear_hover"));
    aboutButton.setHoverTexture(texturesDAO.getTexture("button_info_hover"));
    exitButton.setHoverTexture(texturesDAO.getTexture("button_onoff_hover"));

}

void MainMenu::draw(const float dt){
    app->window.draw(background);
    app->window.draw(title);
    app->window.draw(singlePlayerButton);
    app->window.draw(multiPlayerButton);
    app->window.draw(optionsButton);
    app->window.draw(aboutButton);
    app->window.draw(exitButton);
    return;
}

void MainMenu::update(const float dt){
    receive = network.receivePacket();

    if(receive != packetID::None){
        handlePacket(receive);
    }
}

void MainMenu::handlePacket(packetID packet){

    switch(packet){
        case packetID::LoginResponse:{

            if(network.receiveLoginResponse()){
                app->pushState(new TeamsMenu(app));
            }

            break;
        }

        case packetID::WrongVersion:{
            app->setGlobalMessage("Your game version is not compatible with the server");
        }

    }
}


void MainMenu::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            if(singlePlayerButton.clicked(x, y)){
                app->pushState(new DifficultyMenu(app));
            }
            else if(multiPlayerButton.clicked(x, y)){
                int port = Utility::StringToNumber(configurationDAO.getConfiguration("Port"));
                string server = configurationDAO.getConfiguration("Server");
                if(network.connect(server, port)){
                    network.sendLogin(configurationDAO.getConfiguration("User"), GAME_VERSION);
                }
                else{
                    app->setGlobalMessage("Could not connect to the server");
                }
            }
            else if(optionsButton.clicked(x, y)){
                app->pushState(new ConfigMenu(app));
            }
            else if(aboutButton.clicked(x, y)){
                app->pushState(new About(app));
            }
            else if(exitButton.clicked(x, y)){
                app->window.close();
            }
            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            singlePlayerButton.hover(x, y);
            multiPlayerButton.hover(x, y);
            optionsButton.hover(x, y);
            aboutButton.hover(x, y);
            exitButton.hover(x, y);

            break;
        }
    }
}

