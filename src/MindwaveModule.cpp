#include "MindwaveModule.hpp"

MindwaveModule::MindwaveModule(){
    connectionID = -1;
    lastAttentionValue = -1;
    lastMeditationValue = -1;
    lastBlinkValue = -1;
}

MindwaveModule::~MindwaveModule(){
    disconnect();
}

MindwaveModule& MindwaveModule::getInstance(){
    static MindwaveModule instance;
    return instance;
}

int MindwaveModule::connect(const char* comPortName){

    int errCode = 0;

    cout << "Connecting to port " << comPortName << "..." << endl;

    /* Get a connection ID handle to ThinkGear */
    connectionID = TG_GetNewConnectionId();

    if( connectionID < 0 ) {
        cout << "ERROR: TG_GetNewConnectionId() returned " << connectionID << endl;
        return connectionID;
    }

    /* Set/open stream (raw bytes) log file for connection */
    errCode = TG_SetStreamLog( connectionID, "streamLog.txt" );
    if( errCode < 0 ) {
        cout << "ERROR: TG_SetStreamLog() returned " << errCode << endl;
        connectionID = errCode;
        return connectionID;
    }

    /* Set/open data (ThinkGear values) log file for connection */
    errCode = TG_SetDataLog( connectionID, "dataLog.txt" );
    if( errCode < 0 ) {
        cout << "ERROR: TG_SetDataLog() returned " << errCode << endl;
        connectionID = errCode;
        return connectionID;
    }

    /* Attempt to connect the connection ID handle to serial port "COM5" */
    /* NOTE: On Windows, COM10 and higher must be preceded by \\.\, as in
     *       "\\\\.\\COM12" (must escape backslashes in strings).  COM9
     *       and lower do not require the \\.\, but are allowed to include
     *       them.  On Mac OS X, COM ports are named like
     *       "/dev/tty.MindSet-DevB-1".
     */
    errCode = TG_Connect( connectionID,
                         comPortName,
                         TG_BAUD_57600,
                         TG_STREAM_PACKETS );
    if( errCode < 0 ) {
        cout << "ERROR: TG_Connect() returned " << errCode << endl;
        connectionID = errCode;
        return errCode;
    }

    cout << "Connected: " << connectionID << endl;
    comPort = const_cast<char*>(comPortName);
    enableBlinkDetection();
    enableAutoReading();
    return connectionID;
}

void MindwaveModule::enableAutoReading(){
    if(isConnected()){
        TG_EnableAutoRead(connectionID, true);
    }
}

int MindwaveModule::getAttention(){
    int att = (int)TG_GetValue(connectionID, TG_DATA_ATTENTION);



    if(lastAttentionValue != att){
        cout << "Attention: " << att << endl;
        lastAttentionValue = att;
        return att;
    }
    return 0;
}


int MindwaveModule::getMeditation(){
    int med = (int)TG_GetValue(connectionID, TG_DATA_MEDITATION);

    if(lastMeditationValue != med){
        cout << "Meditation: " << med << endl;
        lastMeditationValue = med;
        return med;
    }

    return 0;
}


int MindwaveModule::getConnectionID(){
    return connectionID;
}


bool MindwaveModule::isConnected(){
    if(connectionID < 0){
		return false;
	}
	else{
        return true;
	}
}

int MindwaveModule::getAlpha(){
    int alpha = 0;
    return alpha;
}

int MindwaveModule::getBeta(){
    int beta = 0;
    return beta;
}

int MindwaveModule::getGamma(){
    int gamma = 0;
    return gamma;
}

int MindwaveModule::getDelta(){
    int delta = 0;
    return delta;
}


int MindwaveModule::getTheta(){
    int theta = 0;
    return theta;
}

int MindwaveModule::getBatteryStrength(){

   return (int)TG_GetValue(connectionID, TG_DATA_BATTERY);

}

/* The signal strength is a number that goes from 0 to 200, indicating the quality of the headset's signal.
However, this value is inverted, i.e., 0 (zero) means a high quality signal, while 200 means that there's no contact
between the electrode and the user's forehead. The higher the value, the poorest the signal.*/

int MindwaveModule::getSignalStrength(){

    return (int)TG_GetValue(connectionID, TG_DATA_POOR_SIGNAL);

}

bool MindwaveModule::hasPoorSignal(){
    if(TG_GetValueStatus(connectionID, TG_DATA_POOR_SIGNAL) != 0){
        if(TG_GetValue(connectionID, TG_DATA_POOR_SIGNAL) > 180){
            return true;
        }
    }
    return false;
}

void MindwaveModule::enableBlinkDetection(){
    if(isConnected()){
        TG_EnableBlinkDetection(connectionID, true);
    }
}

int MindwaveModule::getBlinkStrength(){

    int blink = (int)TG_GetValue(connectionID, TG_DATA_BLINK_STRENGTH);

    if(lastBlinkValue != blink){

        cout << "Blink: " << blink << endl;

        lastBlinkValue = blink;
        return blink;
    }

    return 0;

}


void MindwaveModule::disconnect(){
    TG_FreeConnection(connectionID);
    connectionID = -1;
}
