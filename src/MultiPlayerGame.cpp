#include "MultiPlayerGame.hpp"

MultiPlayerGame::MultiPlayerGame(std::vector<Player*> firstTeam,
                                 std::vector<Player*> secondTeam) : leftTeam(firstTeam),
                                                                    rightTeam(secondTeam, true){

    leftTeam.setPosition  (sf::Vector2f(SCREEN_LENGHT*0.30, SCREEN_HEIGHT*0.70));
    rightTeam.setPosition (sf::Vector2f(SCREEN_LENGHT*0.70, SCREEN_HEIGHT*0.70));

    leftTeam.focusMeter.setPosition (sf::Vector2f(LFOCUS_METER_POS_X, FOCUS_METER_POS_Y));
    rightTeam.focusMeter.setPosition(sf::Vector2f(RFOCUS_METER_POS_X, FOCUS_METER_POS_Y));

    leftTeam.setLeft();

    background.setTexture(texturesDAO.getTexture("background_snow"));

    division.setTexture(texturesDAO.getTexture("division_snow"));
    division.setPosition(SCREEN_LENGHT/2 - division.getTexture()->getSize().x/2, SCREEN_HEIGHT*0.8);

    forceMeter.setNumberOfPlayersTeamOne(leftTeam.getSize());
    forceMeter.setNumberOfPlayersTeamTwo(rightTeam.getSize());

    switch(leftTeam.getSize()){
        case 1:
            leftRope.setTexture(texturesDAO.getTexture("left_rope_short"));
        break;

        case 2:
            leftRope.setTexture(texturesDAO.getTexture("left_rope_medium"));
        break;

        case 3:
            leftRope.setTexture(texturesDAO.getTexture("left_rope_large"));
        break;
    }

    switch(rightTeam.getSize()){
        case 1:
            rightRope.setTexture(texturesDAO.getTexture("right_rope_short"));
        break;

        case 2:
            rightRope.setTexture(texturesDAO.getTexture("right_rope_medium"));
        break;

        case 3:
            rightRope.setTexture(texturesDAO.getTexture("right_rope_large"));
        break;
    }

    leftRope.setPosition(SCREEN_LENGHT/2 - leftRope.getTexture()->getSize().x + 20,
                         SCREEN_HEIGHT*0.765);

    rightRope.setPosition(SCREEN_LENGHT/2,
                          SCREEN_HEIGHT*0.765);

    updateControl = true;
}

MultiPlayerGame::~MultiPlayerGame(){
    //dtor
}

void MultiPlayerGame::draw(const float dt){
    if(GUI){
        app->window.draw(background);
        app->window.draw(division);
        app->window.draw(forceMeter);

        app->window.draw(leftTeam);
        app->window.draw(rightTeam);

        app->window.draw(leftRope);
        app->window.draw(rightRope);
        app->window.draw(ropeStripe);

        leftTeam.drawArmsAndFingers(app->window);
        rightTeam.drawArmsAndFingers(app->window);

        //app->window.draw(middleLine);

        if(GameState == GAMESTATE::OVER){
            app->window.draw(gameOverBackground);
            app->window.draw(gameOverBoard);
            app->window.draw(gameOverText);
        }

        app->window.draw(alertText);

        if(GameState == GAMESTATE::COUNTDOWN){
            app->window.draw(counterSprite);
            app->window.draw(counterText);
        }
    }
    else{
        app->window.draw(noGUIbackground);
    }
}

void MultiPlayerGame::update(const float dt){

    receive = network.receivePacket();

    if(receive != packetID::None){
        handlePacket(receive);
    }

    switch (GameState){
        case GAMESTATE::COUNTDOWN:
            updateCounter(dt);
        break;

        case GAMESTATE::PLAYING:
            updateGame(dt);
        break;

        case GAMESTATE::OVER:
            updateGameover(dt);
        break;

        case GAMESTATE::WAIT:
            //wait
        break;
    }
}

void MultiPlayerGame::handlePacket(packetID packet){
    switch(packet){
        case packetID::Attention:{
            //transfer the attention of each user to its object and update it
            int id, att;
            network.receiveAttention(att, id);
            leftTeam.setForce   (id, att);
            rightTeam.setForce  (id, att);
            break;
        }
        case packetID::GameWin : {

            int avg, avgTeam;

            network.receiveAverages(avg, avgTeam);

            gameOverText.setColor(sf::Color::Green);
            gameOverText.setString("Your team won!\nAverage attention: " + Utility::toString(avg) +
                                   "\nTeam average attention: " + Utility::toString(avgTeam));

            gameOverText.setPosition(SCREEN_LENGHT/2 - gameOverText.getLocalBounds().width/2,
                                     SCREEN_HEIGHT/2 - gameOverText.getLocalBounds().height/2);
            counterClock.restart();
            GameState = GAMESTATE::OVER;
        }
        break;

        case packetID::GameLose: {

            int avg, avgTeam;

            network.receiveAverages(avg, avgTeam);

            gameOverText.setColor(sf::Color::Red);
            gameOverText.setString("Your team lost!\nAverage attention: " + Utility::toString(avg) +
                                   "\nTeam average attention: " + Utility::toString(avgTeam));

            gameOverText.setPosition(SCREEN_LENGHT/2 - gameOverText.getLocalBounds().width/2,
                                     SCREEN_HEIGHT/2 - gameOverText.getLocalBounds().height/2);
            counterClock.restart();
            GameState = GAMESTATE::OVER;
        }
        break;

    }
}


void MultiPlayerGame::updateGame(const float dt){

    Game::update(dt);

    if(not gameover){

        att = mindwaveModule.getAttention();

        if(att > 0){
            network.sendAttention(att);
        }

        leftTeam.update();
        rightTeam.update();

        if(updateClock.getElapsedTime().asSeconds() > 1){

            if(rightTeam.getTotalForce() > leftTeam.getTotalForce()){
                resultant = std::min(rightTeam.getTotalForce() - leftTeam.getTotalForce(), 20);
            }
            else{
                resultant = std::max(rightTeam.getTotalForce() - leftTeam.getTotalForce(), -20);
            }

            force = sf::Vector2f(resultant/(SCREEN_LENGHT*0.2), 0);

            forceMeter.setForce(resultant);

            leftTeam.applyForce(force);
            rightTeam.applyForce(force);

            leftRope.setPosition(leftRope.getPosition() + force);
            rightRope.setPosition(rightRope.getPosition() + force);
            ropeStripe.setPosition(ropeStripe.getPosition() + force);

            updateExpressions();
        }
        else{
            updateControl = false;

            if(updateClock.getElapsedTime().asSeconds() > 4){
                updateControl = true;
                updateClock.restart();
            }

        }

        blinkStrength = mindwaveModule.getBlinkStrength();

        if(blinkStrength > 32){
            network.sendBlink(blinkStrength);
        }

        testGameover();

    }
    else{
        network.sendGameEnd(winner);
        GameState = GAMESTATE::WAIT;
    }
}

void MultiPlayerGame::updateCounter(const float dt){
    int counterTime = static_cast<int>(counterClock.getElapsedTime().asSeconds());

    counterText.setString(Utility::toString(3 - counterTime));

    if(counterClock.getElapsedTime().asMilliseconds() >= 3000){
        GameState = GAMESTATE::PLAYING;
        counterText.setString("");
    }
}


void MultiPlayerGame::updateGameover(const float dt){
    if(counterClock.getElapsedTime().asSeconds() > 5){
        app->popState();
    }
}


void MultiPlayerGame::handleEvent(sf::Event& event){
    switch(event.type){

        case sf::Event::Closed:
            network.sendDisconnect();
            app->window.close();
        break;

        case sf::Event::KeyPressed:
            char input = static_cast<char>(event.text.unicode);
            if(Utility::StringToNumber(configurationDAO.getConfiguration("ManualGame"))){
                switch(input){

                    case sf::Keyboard::Right:
                        if(rightTeam.getTotalForce() + 1 <= MAX_FORCE){
                            rightTeam.setForce(0, rightTeam.getPlayers().at(0)->getForce() + 1);
                        }
                    break;

                    case sf::Keyboard::Left:
                        if(rightTeam.getTotalForce() - 1 >= MIN_FORCE){
                            rightTeam.setForce(0, rightTeam.getPlayers().at(0)->getForce() - 1);
                        }
                    break;

                    case sf::Keyboard::A:
                        if(leftTeam.getTotalForce() + 1 <= MAX_FORCE){
                            network.sendAttention(85);
                        }
                    break;

                    case sf::Keyboard::D:
                        if(leftTeam.getTotalForce() - 1 >= MIN_FORCE){
                            leftTeam.setForce(0, leftTeam.getPlayers().at(0)->getForce() - 1);
                        }
                    break;
                }
            }
        break;
    }
}

void MultiPlayerGame::testGameover(){
    if(leftTeam.getPosition().x  + PLAYER_SIZE/2 > SCREEN_LENGHT/2){
        winner = 2;
        gameover = true;
    }

    if(rightTeam.getPosition().x - PLAYER_SIZE/2 < SCREEN_LENGHT/2){
        winner = 1;
        gameover = true;
    }
}


void MultiPlayerGame::updateExpressions(){
    for(auto& player : leftTeam.getPlayers()){
        if(player->getAverageForce() >= 85){
            player->setExpression(EXPRESSION::ANGRY);
        }
        else if(leftTeam.getPosition().x + PLAYER_SIZE/2 > SCREEN_LENGHT*0.45){
            player->setExpression(EXPRESSION::DESPERATE);
        }
        else{
            player->setExpression(EXPRESSION::REGULAR);
        }
    }

    for(auto& player : rightTeam.getPlayers()){
        if(player->getAverageForce() >= 85){
            player->setExpression(EXPRESSION::ANGRY);
        }
        else if(rightTeam.getPosition().x - PLAYER_SIZE/2 < SCREEN_LENGHT*0.55){
            player->setExpression(EXPRESSION::DESPERATE);
        }
        else{
            player->setExpression(EXPRESSION::REGULAR);
        }
    }
}
