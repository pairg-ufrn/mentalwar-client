#include "focusMeter.hpp"

FocusMeter::FocusMeter() {
    background.setTexture(texturesDAO.getTexture("focus_meter_background"));
    meterSprite.setTexture(texturesDAO.getTexture("focus_meter"));

    sf::Vector2f newSize(meterSprite.getTexture()->getSize());
    m_size = newSize;
    m_body.setSize(m_size);
    m_body.setFillColor(sf::Color::White);

    //center origins
    meterSprite.setOrigin(meterSprite.getGlobalBounds().left + meterSprite.getGlobalBounds().width/2,
                          meterSprite.getGlobalBounds().top + meterSprite.getGlobalBounds().height/2);

    m_body.setOrigin(m_body.getGlobalBounds().left + m_body.getGlobalBounds().width/2,
                     m_body.getGlobalBounds().top + m_body.getGlobalBounds().height/2);

}

void FocusMeter::addPlayer(Player* _player, bool _right) {
    right = _right;
    string orientation = _right? "_right" : "";

    players.push_back(_player);

    sf::Sprite newMeter;
    newMeter.setTexture(texturesDAO.getTexture("meter_arrow_horizontal" + orientation));
    newMeter.setPosition(m_body.getPosition().x - m_body.getSize().x - 10*right,
                         m_body.getPosition().y + m_body.getSize().y/2 - 16);
    meters.push_back(newMeter);

    sf::Sprite newPointer;
    newPointer.setTexture(texturesDAO.getTexture("null"));
    pointers.push_back(newPointer);
}

void FocusMeter::addPlayers(vector<Player*> _players, bool _right) {
    right = _right;

    int n = _players.size();

    for(auto& player : _players){
        players.push_back(player);

        sf::Sprite newMeter;
        newMeter.setTexture(texturesDAO.getTexture("pointer_" + player->getColorName() + "_" + player->getOrientation()));
        newMeter.setPosition(m_body.getPosition().x - m_body.getSize().x - 10*right,
                             m_body.getPosition().y + m_body.getSize().y/2 - 16);
        meters.push_back(newMeter);

        sf::Sprite newPointer;
        newPointer.setTexture(texturesDAO.getTexture("pointer_" + player->getColorName()));
        newPointer.setPosition(player->getPosition().x,
                               player->getPosition().y - PLAYER_SIZE/2 - 86);
       pointers.push_back(newPointer);
    }
}

void FocusMeter::draw(sf::RenderTarget& target, sf::RenderStates states) const {

    target.draw(background);
    target.draw(meterSprite);
    //target.draw(meter);

    //target.draw(m_body);

    for(auto meter : meters){
        target.draw(meter);
    }

    for(auto pointer : pointers){
        target.draw(pointer);
    }

}

void FocusMeter::update() {

    float bottom = m_body.getGlobalBounds().top + m_body.getSize().y;

    for(int i = 0; i < players.size(); i++){

        float newPos = bottom - (m_body.getSize().y*players[i]->getForce())/100.0f;

        meters[i].setPosition(m_body.getPosition().x - m_body.getSize().x -12*right,
                              newPos - 12); //Keep the X position, changes only height
        pointers[i].setPosition(players[i]->getPosition().x,
                                players[i]->getPosition().y - PLAYER_SIZE/2 - 86);
    }
}

sf::Vector2f FocusMeter::getSize(){
    return m_body.getSize();
}

void FocusMeter::setPosition(sf::Vector2f pos){
    background.setPosition(pos);

    meterSprite.setPosition(pos.x + background.getLocalBounds().width/2,
                            pos.y + background.getLocalBounds().height/2);
    m_body.setPosition(meterSprite.getPosition());

    update();
}

void FocusMeter::clearAll(){
    players.clear();
    meters.clear();
    pointers.clear();
}
