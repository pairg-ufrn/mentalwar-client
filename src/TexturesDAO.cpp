#include "TexturesDAO.h"

TexturesDAO::TexturesDAO(){
    //ctor
}

TexturesDAO::~TexturesDAO(){
    //dtor
}

TexturesDAO& TexturesDAO::getInstance(){
    static TexturesDAO instance;
    return instance;
}

void TexturesDAO::loadTextures(){

    loadTexture("null", "null.png");

    loadTexture("about", "about.png");

    loadTexture("background", "backgrounds/background.png");
    loadTexture("background_menu", "backgrounds/background_menu.png");
    loadTexture("background_snow", "backgrounds/background_snow.png");
    loadTexture("background_plains", "backgrounds/background_plains.png");
    loadTexture("background_desert", "backgrounds/background_desert.png");
    loadTexture("background_volcano", "backgrounds/background_volcano.png");
    loadTexture("background_gameover", "backgrounds/background_gameover.png");

    loadTexture("button_1", "buttons/button_1.png");
    loadTexture("button_1_hover", "buttons/button_1_hover.png");

    loadTexture("button_2", "buttons/button_2.png");
    loadTexture("button_2_hover", "buttons/button_2_hover.png");

    loadTexture("button_onoff", "buttons/button_onoff.png");
    loadTexture("button_onoff_hover", "buttons/button_onoff_hover.png");

    loadTexture("button_gear", "buttons/button_gear.png");
    loadTexture("button_gear_hover", "buttons/button_gear_hover.png");

    loadTexture("button_info", "buttons/button_info.png");
    loadTexture("button_info_hover", "buttons/button_info_hover.png");

    loadTexture("button_sound", "buttons/button_sound.png");
    loadTexture("button_mute", "buttons/button_mute.png");

    loadTexture("button_male", "buttons/button_male.png");
    loadTexture("button_male_hover", "buttons/button_male_hover.png");

    loadTexture("button_female", "buttons/button_female.png");
    loadTexture("button_female_hover", "buttons/button_female_hover.png");

    loadTexture("button_arrow_left", "buttons/button_arrow_left.png");
    loadTexture("button_arrow_left_hover", "buttons/button_arrow_left_hover.png");

    loadTexture("button_arrow_right", "buttons/button_arrow_right.png");
    loadTexture("button_arrow_right_hover", "buttons/button_arrow_right_hover.png");

    loadTexture("button_back", "buttons/button_back.png");
    loadTexture("button_back_hover", "buttons/button_back_hover.png");

    loadTexture("button_easy", "buttons/button_easy.png");
    loadTexture("button_medium", "buttons/button_medium.png");
    loadTexture("button_hard", "buttons/button_hard.png");

    loadTexture("counter_circle", "counter_circle.png");

    loadTexture("division_plains", "backgrounds/division_plains.png");
    loadTexture("division_desert", "backgrounds/division_desert.png");
    loadTexture("division_volcano", "backgrounds/division_volcano.png");
    loadTexture("division_snow", "backgrounds/division_snow.png");

    loadTexture("focus_meter", "focus_meter.png");
    loadTexture("focus_meter_background", "focus_meter_background.png");

    loadTexture("force_meter", "force_meter.png");
    loadTexture("force_meter_background", "force_meter_background.png");
    loadTexture("force_meter_team_one", "force_meter_team_one.png");
    loadTexture("force_meter_team_two", "force_meter_team_two.png");
    loadTexture("force_meter_circle", "force_meter_circle.png");

    loadTexture("headset_connected", "connected_v1.png");
    loadTexture("headset_disconnected", "nosignal_v1.png");

    loadTexture("headset_signal_none", "headset_signal_none.png");
    loadTexture("headset_signal_low", "headset_signal_low.png");
    loadTexture("headset_signal_medium", "headset_signal_medium.png");
    loadTexture("headset_signal_high", "headset_signal_high.png");

    loadTexture("icon", "icon.png");
    loadTexture("icon_male_1_left", "icons/icon_male_1_left.png");
    loadTexture("icon_male_2_left", "icons/icon_male_2_left.png");
    loadTexture("icon_male_3_left", "icons/icon_male_3_left.png");
    loadTexture("icon_male_4_left", "icons/icon_male_4_left.png");
    loadTexture("icon_female_1_left", "icons/icon_female_1_left.png");
    loadTexture("icon_female_2_left", "icons/icon_female_2_left.png");
    loadTexture("icon_computer", "icon_computer.png");
    loadTexture("icon_one_player", "icon_one_player.png");
    loadTexture("icon_two_players", "icon_two_players.png");
    loadTexture("icon_three_players", "icon_three_players.png");

    loadTexture("no_GUI", "no_GUI.png");

    loadTexture("meter_arrow_horizontal_right", "meter_arrow_horizontal_right.png");
    loadTexture("meter_arrow_horizontal", "meter_arrow_horizontal.png");
    loadTexture("meter_arrow_vertical", "meter_arrow_vertical.png");

    loadTexture("left_rope_short", "left_rope_short.png");
    loadTexture("right_rope_short", "right_rope_short.png");
    loadTexture("left_rope_medium", "left_rope_medium.png");
    loadTexture("right_rope_medium", "right_rope_medium.png");
    loadTexture("left_rope_large", "left_rope_large.png");
    loadTexture("right_rope_large", "right_rope_large.png");

    loadTexture("pointer_blue", "pointers/pointer_blue.png");
    loadTexture("pointer_blue_left", "pointers/pointer_blue_left.png");
    loadTexture("pointer_blue_right", "pointers/pointer_blue_right.png");

    loadTexture("pointer_cyan", "pointers/pointer_cyan.png");
    loadTexture("pointer_cyan_left", "pointers/pointer_cyan_left.png");
    loadTexture("pointer_cyan_right", "pointers/pointer_cyan_right.png");

    loadTexture("pointer_green", "pointers/pointer_green.png");
    loadTexture("pointer_green_left", "pointers/pointer_green_left.png");
    loadTexture("pointer_green_right", "pointers/pointer_green_right.png");

    loadTexture("pointer_purple", "pointers/pointer_purple.png");
    loadTexture("pointer_purple_left", "pointers/pointer_purple_left.png");
    loadTexture("pointer_purple_right", "pointers/pointer_purple_right.png");

    loadTexture("pointer_red", "pointers/pointer_red.png");
    loadTexture("pointer_red_left", "pointers/pointer_red_left.png");
    loadTexture("pointer_red_right", "pointers/pointer_red_right.png");

    loadTexture("pointer_white", "pointers/pointer_white.png");
    loadTexture("pointer_white_left", "pointers/pointer_white_left.png");
    loadTexture("pointer_white_right", "pointers/pointer_white_right.png");

    loadTexture("rope_stripe", "rope_stripe.png");

    loadTexture("eye_opened", "eye_opened.png");
    loadTexture("eye_closed", "eye_closed.png");

    loadTexture("ready", "ready.png");
    loadTexture("not_ready", "not_ready.png");

    loadCharacters();

    loadTexture("selection_board", "selection_board.png");
    loadTexture("team_board", "team_board.png");
    loadTexture("gameover_board", "gameover_board.png");

    loadTexture("player_holder_bar", "player_holder_bar.png");

    return;
}


void TexturesDAO::loadCharacters(){
    //Heads
    loadTexture("character_male_head_1_1", "characters/male/heads/head_1_1.png");
    loadTexture("character_male_head_1_2", "characters/male/heads/head_1_2.png");
    loadTexture("character_male_head_1_3", "characters/male/heads/head_1_3.png");
    loadTexture("character_male_head_1_4", "characters/male/heads/head_1_4.png");

    loadTexture("character_male_head_2_1", "characters/male/heads/head_2_1.png");
    loadTexture("character_male_head_2_2", "characters/male/heads/head_2_2.png");
    loadTexture("character_male_head_2_3", "characters/male/heads/head_2_3.png");
    loadTexture("character_male_head_2_4", "characters/male/heads/head_2_4.png");

    loadTexture("character_male_head_3_1", "characters/male/heads/head_3_1.png");
    loadTexture("character_male_head_3_2", "characters/male/heads/head_3_2.png");
    loadTexture("character_male_head_3_3", "characters/male/heads/head_3_3.png");
    loadTexture("character_male_head_3_4", "characters/male/heads/head_3_4.png");

    loadTexture("character_male_head_4_1", "characters/male/heads/head_4_1.png");
    loadTexture("character_male_head_4_2", "characters/male/heads/head_4_2.png");
    loadTexture("character_male_head_4_3", "characters/male/heads/head_4_3.png");
    loadTexture("character_male_head_4_4", "characters/male/heads/head_4_4.png");

    loadTexture("character_female_head_1_1", "characters/female/heads/head_1_1.png");
    loadTexture("character_female_head_1_2", "characters/female/heads/head_1_2.png");
    loadTexture("character_female_head_1_3", "characters/female/heads/head_1_3.png");
    loadTexture("character_female_head_1_4", "characters/female/heads/head_1_4.png");

    loadTexture("character_female_head_2_1", "characters/female/heads/head_2_1.png");
    loadTexture("character_female_head_2_2", "characters/female/heads/head_2_2.png");
    loadTexture("character_female_head_2_3", "characters/female/heads/head_2_3.png");
    loadTexture("character_female_head_2_4", "characters/female/heads/head_2_4.png");

    loadTexture("character_male_head_1_1_left", "characters/male/heads/head_1_1_left.png");
    loadTexture("character_male_head_1_2_left", "characters/male/heads/head_1_2_left.png");
    loadTexture("character_male_head_1_3_left", "characters/male/heads/head_1_3_left.png");
    loadTexture("character_male_head_1_4_left", "characters/male/heads/head_1_4_left.png");

    loadTexture("character_male_head_2_1_left", "characters/male/heads/head_2_1_left.png");
    loadTexture("character_male_head_2_2_left", "characters/male/heads/head_2_2_left.png");
    loadTexture("character_male_head_2_3_left", "characters/male/heads/head_2_3_left.png");
    loadTexture("character_male_head_2_4_left", "characters/male/heads/head_2_4_left.png");

    loadTexture("character_male_head_3_1_left", "characters/male/heads/head_3_1_left.png");
    loadTexture("character_male_head_3_2_left", "characters/male/heads/head_3_2_left.png");
    loadTexture("character_male_head_3_3_left", "characters/male/heads/head_3_3_left.png");
    loadTexture("character_male_head_3_4_left", "characters/male/heads/head_3_4_left.png");

    loadTexture("character_male_head_4_1_left", "characters/male/heads/head_4_1_left.png");
    loadTexture("character_male_head_4_2_left", "characters/male/heads/head_4_2_left.png");
    loadTexture("character_male_head_4_3_left", "characters/male/heads/head_4_3_left.png");
    loadTexture("character_male_head_4_4_left", "characters/male/heads/head_4_4_left.png");

    loadTexture("character_female_head_1_1_left", "characters/female/heads/head_1_1_left.png");
    loadTexture("character_female_head_1_2_left", "characters/female/heads/head_1_2_left.png");
    loadTexture("character_female_head_1_3_left", "characters/female/heads/head_1_3_left.png");
    loadTexture("character_female_head_1_4_left", "characters/female/heads/head_1_4_left.png");

    loadTexture("character_female_head_2_1_left", "characters/female/heads/head_2_1_left.png");
    loadTexture("character_female_head_2_2_left", "characters/female/heads/head_2_2_left.png");
    loadTexture("character_female_head_2_3_left", "characters/female/heads/head_2_3_left.png");
    loadTexture("character_female_head_2_4_left", "characters/female/heads/head_2_4_left.png");

    //Bodies
    loadTexture("character_male_body_1", "characters/male/bodies/body_1.png");
    loadTexture("character_male_body_2", "characters/male/bodies/body_2.png");
    loadTexture("character_male_body_3", "characters/male/bodies/body_3.png");
    loadTexture("character_male_body_4", "characters/male/bodies/body_4.png");

    loadTexture("character_female_body_1", "characters/female/bodies/body_1.png");
    loadTexture("character_female_body_2", "characters/female/bodies/body_2.png");

    loadTexture("character_male_body_1_left", "characters/male/bodies/body_1_left.png");
    loadTexture("character_male_body_2_left", "characters/male/bodies/body_2_left.png");
    loadTexture("character_male_body_3_left", "characters/male/bodies/body_3_left.png");
    loadTexture("character_male_body_4_left", "characters/male/bodies/body_4_left.png");

    loadTexture("character_female_body_1_left", "characters/female/bodies/body_1_left.png");
    loadTexture("character_female_body_2_left", "characters/female/bodies/body_2_left.png");

    //Arms
    loadTexture("character_male_arms_1", "characters/male/arms/arms_1.png");
    loadTexture("character_male_arms_2", "characters/male/arms/arms_2.png");
    loadTexture("character_male_arms_3", "characters/male/arms/arms_3.png");
    loadTexture("character_male_arms_4", "characters/male/arms/arms_4.png");

    loadTexture("character_female_arms_1", "characters/female/arms/arms_1.png");
    loadTexture("character_female_arms_2", "characters/female/arms/arms_2.png");

    loadTexture("character_male_arms_1_left", "characters/male/arms/arms_1_left.png");
    loadTexture("character_male_arms_2_left", "characters/male/arms/arms_2_left.png");
    loadTexture("character_male_arms_3_left", "characters/male/arms/arms_3_left.png");
    loadTexture("character_male_arms_4_left", "characters/male/arms/arms_4_left.png");

    loadTexture("character_female_arms_1_left", "characters/female/arms/arms_1_left.png");
    loadTexture("character_female_arms_2_left", "characters/female/arms/arms_2_left.png");

    //Fingers
    loadTexture("character_male_fingers", "characters/male_fingers.png");
    loadTexture("character_female_fingers", "characters/female_fingers.png");

    loadTexture("character_male_fingers_left", "characters/male_fingers_left.png");
    loadTexture("character_female_fingers_left", "characters/female_fingers_left.png");

    //Legs
    loadTexture("character_male_left_leg_1", "characters/male/legs/left_leg_1.png");
    loadTexture("character_male_right_leg_1", "characters/male/legs/right_leg_1.png");

    loadTexture("character_male_left_leg_2", "characters/male/legs/left_leg_2.png");
    loadTexture("character_male_right_leg_2", "characters/male/legs/right_leg_2.png");

    loadTexture("character_male_left_leg_3", "characters/male/legs/left_leg_3.png");
    loadTexture("character_male_right_leg_3", "characters/male/legs/right_leg_3.png");

    loadTexture("character_male_left_leg_4", "characters/male/legs/left_leg_4.png");
    loadTexture("character_male_right_leg_4", "characters/male/legs/right_leg_4.png");

    loadTexture("character_female_left_leg_1", "characters/female/legs/left_leg_1.png");
    loadTexture("character_female_right_leg_1", "characters/female/legs/right_leg_1.png");

    loadTexture("character_female_left_leg_2", "characters/female/legs/left_leg_2.png");
    loadTexture("character_female_right_leg_2", "characters/female/legs/right_leg_2.png");

    loadTexture("character_male_left_leg_1_left", "characters/male/legs/left_leg_1_left.png");
    loadTexture("character_male_right_leg_1_left", "characters/male/legs/right_leg_1_left.png");

    loadTexture("character_male_left_leg_2_left", "characters/male/legs/left_leg_2_left.png");
    loadTexture("character_male_right_leg_2_left", "characters/male/legs/right_leg_2_left.png");

    loadTexture("character_male_left_leg_3_left", "characters/male/legs/left_leg_3_left.png");
    loadTexture("character_male_right_leg_3_left", "characters/male/legs/right_leg_3_left.png");

    loadTexture("character_male_left_leg_4_left", "characters/male/legs/left_leg_4_left.png");
    loadTexture("character_male_right_leg_4_left", "characters/male/legs/right_leg_4_left.png");

    loadTexture("character_female_left_leg_1_left", "characters/female/legs/left_leg_1_left.png");
    loadTexture("character_female_right_leg_1_left", "characters/female/legs/right_leg_1_left.png");

    loadTexture("character_female_left_leg_2_left", "characters/female/legs/left_leg_2_left.png");
    loadTexture("character_female_right_leg_2_left", "characters/female/legs/right_leg_2_left.png");

    //Shadow
    loadTexture("character_shadow", "characters/shadow.png");
    loadTexture("character_shadow_left", "characters/shadow_left.png");
}


sf::Texture& TexturesDAO::getTexture(const std::string& textureName){
    try{
        return this->textures.at(textureName);
    }
    catch(std::out_of_range& e){
        std::cout << "Texture " << textureName << " not found." << std::endl;
        return textures.at("null");
    }
}


void TexturesDAO::loadTexture(const std::string& name, const std::string& filename){
    sf::Texture texture;
    texture.loadFromFile("media/textures/" + filename);

    this->textures[name] = texture;

    return;
}
