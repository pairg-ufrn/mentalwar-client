#include "difficultyMenu.hpp"

DifficultyMenu::DifficultyMenu(Application* app):   easyButton(336, 200, 124, 48, "Easy"),
                                                    mediumButton(336, 270, 124, 48, "Medium"),
                                                    hardButton(336, 340, 124, 48, "Hard"),
                                                    backButton(175, 450, 48, 48, ""),
                                                    playButton(425, 450, 128, 48, "Play!"){
    this->app = app;

    background.setTexture(texturesDAO.getTexture("background_menu"));

    title.setCharacterSize(96U);
    title.setColor(sf::Color::White);
    title.setFont(fontsDAO.getFont("life_is_goofy"));
    title.setString("Difficulty");
    title.setPosition(SCREEN_LENGHT/2 - title.getLocalBounds().width/2, SCREEN_HEIGHT*0.05);

    easyButton.setColor(sf::Color::Green);
    easyButton.disableHover();
    easyButton.setFontSize(20U);

    mediumButton.setColor(sf::Color::Yellow);
    mediumButton.disableHover();
    mediumButton.setFontSize(20U);

    hardButton.setColor(sf::Color::Red);
    hardButton.disableHover();
    hardButton.setFontSize(20U);

    playButton.setTexture(texturesDAO.getTexture("button_1"));
    backButton.setTexture(texturesDAO.getTexture("button_back"));

    playButton.setHoverTexture(texturesDAO.getTexture("button_1_hover"));
    backButton.setHoverTexture(texturesDAO.getTexture("button_back_hover"));

    easyButton.setTexture(texturesDAO.getTexture("button_easy"));
    mediumButton.setTexture(texturesDAO.getTexture("button_medium"));
    hardButton.setTexture(texturesDAO.getTexture("button_hard"));

    playButton.disable();

}

void DifficultyMenu::draw(const float dt){
    app->window.draw(background);
    app->window.draw(title);
    app->window.draw(easyButton);
    app->window.draw(mediumButton);
    app->window.draw(hardButton);
    app->window.draw(backButton);
   // app->window.draw(playButton);
    return;
}

void DifficultyMenu::update(const float dt){

}

void DifficultyMenu::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            if(easyButton.clicked(x, y)){
               // selectedDifficulty = 0;
                app->pushState(new Playing(app, new SinglePlayerGame(0)));
            }
            else if(mediumButton.clicked(x, y)){
               // selectedDifficulty = 1;
                app->pushState(new Playing(app, new SinglePlayerGame(1)));
            }
            else if(hardButton.clicked(x, y)){
                //selectedDifficulty = 2;
                app->pushState(new Playing(app, new SinglePlayerGame(2)));
            }
            else if(backButton.clicked(x, y)){
                app->popState();
            }
            else if(playButton.clicked(x, y)){
               // app->pushState(new Playing(app, new SinglePlayerGame(selectedDifficulty)));
            }
            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            easyButton.hover(x, y);
            mediumButton.hover(x, y);
            hardButton.hover(x, y);
            backButton.hover(x, y);
            playButton.hover(x, y);

            break;
        }
    }
}
