#include "ConfigMenu.hpp"

ConfigMenu::ConfigMenu(Application* app) :  soundButton(336, 200, 64, 64, ""),
                                            saveButton(450, 530, 128, 48, "Save"),
                                            cancelButton(220, 530, 128, 48, "Cancel"){
    this->app = app;

    background.setTexture(texturesDAO.getTexture("background_menu"));

//    soundText.setCharacterSize(30U);
//    soundText.setString("Sound");
//    soundText.setColor(sf::Color::White);
//    soundText.setFont(fontsDAO.getFont("agency_bold"));
//    soundText.setPosition(SCREEN_LENGHT*0.1, SCREEN_HEIGHT*0.2);

    sound = Utility::StringToNumber(configurationDAO.getConfiguration("Audio"));

    soundButton.setPosition(SCREEN_LENGHT*0.1, SCREEN_HEIGHT*0.2);
    soundButton.setColor(sf::Color::Green);
    soundButton.disableHover();

    if(sound){
        soundButton.setTexture(texturesDAO.getTexture("button_sound"));
    }
    else{
        soundButton.setTexture(texturesDAO.getTexture("button_mute"));
    }

    saveButton.setTexture(texturesDAO.getTexture("button_1"));
    saveButton.setHoverTexture(texturesDAO.getTexture("button_1_hover"));

    cancelButton.setTexture(texturesDAO.getTexture("button_1"));
    cancelButton.setHoverTexture(texturesDAO.getTexture("button_1_hover"));

    playerNameTextBox.setTextColor(sf::Color::White);
    playerNameTextBox.setSize(150, 32);
    playerNameTextBox.setPosition(SCREEN_LENGHT*0.5 - playerNameTextBox.getSize().x/2, SCREEN_HEIGHT*0.12);
    playerNameTextBox.setText(configurationDAO.getConfiguration("User"));

   // characterSelector.setPosition(sf::Vector2f(SCREEN_LENGHT*0.3, SCREEN_HEIGHT*0.4));

}

ConfigMenu::~ConfigMenu(){
    //dtor
}

void ConfigMenu::draw(const float dt){

    app->window.draw(background);

    app->window.draw(soundText);
    app->window.draw(characterSelector);
    app->window.draw(playerNameTextBox);

    app->window.draw(soundButton);
    app->window.draw(saveButton);
    app->window.draw(cancelButton);

    return;
}

void ConfigMenu::update(const float dt){

}

void ConfigMenu::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            if(soundButton.clicked(x, y)){
                sound = !sound;
                if(sound){
                    //soundButton.setText("ON");
                    soundButton.setTexture(texturesDAO.getTexture("button_sound"));
                    soundButton.setColor(sf::Color::Green);
                    soundPlayer.setGlobalVolume(100);
                }
                else{
                    //soundButton.setText("OFF");
                    soundButton.setTexture(texturesDAO.getTexture("button_mute"));
                    soundButton.setColor(sf::Color::Red);
                    soundPlayer.setGlobalVolume(0);
                }
            }

            else if(playerNameTextBox.clicked(x, y)){
                playerNameTextBox.select();
            }

            else if(saveButton.clicked(x, y)){
                saveConfigurations();
                app->popState();
            }
            else if(cancelButton.clicked(x, y)){
                app->popState();
            }

            characterSelector.handleEvent(event);

            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            soundButton.hover(x, y);
            saveButton.hover(x, y);
            cancelButton.hover(x, y);

            characterSelector.handleEvent(event);

            break;
        }

        case sf::Event::TextEntered: {
            playerNameTextBox.processUserInput(event);
        }
    }
}

void ConfigMenu::saveConfigurations(){
    if(playerNameTextBox.getString().size() > 0){
        playerNameTextBox.removeWhiteSpaces();
        configurationDAO.setUser(playerNameTextBox.getString());
    }

    configurationDAO.setAudio(sound);
    configurationDAO.setHead(characterSelector.getCurrentHead());
    configurationDAO.setBody(characterSelector.getCurrentBody());
    configurationDAO.setLegs(characterSelector.getCurrentLegs());
    configurationDAO.setSex(characterSelector.getSex());

    cout << "Configurations saved" << endl;
}
