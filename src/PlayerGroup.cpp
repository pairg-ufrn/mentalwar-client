#include "PlayerGroup.hpp"

PlayerGroup::PlayerGroup(std::vector<Player*> _players, bool right){
    for(auto& player : _players){
        addPlayer(player);
    }
    focusMeter.addPlayers(_players, right);
}


PlayerGroup::~PlayerGroup(){
    //dtor
}


void PlayerGroup::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    for(auto& player: players){
        player->draw(target, states);
    }

    target.draw(focusMeter);
}

void PlayerGroup::drawArmsAndFingers(sf::RenderTarget& target){
    for(auto& player: players){
        player->drawArmsAndFingers(target);
    }
}



void PlayerGroup::addPlayer(Player* player){
    players.push_back(player);
}


void PlayerGroup::update(){
    for(auto& player: players){
        player->update();
    }
    focusMeter.update();
}


void PlayerGroup::applyForce(sf::Vector2f _force){
    setPosition(getPosition() + _force);
}


void PlayerGroup::setForce(int playerID, int force){
    for(auto& player : players){
        cout << "PlayerID: " << player->getID() << endl;
        if(player->getID() == playerID){
            cout << "Player: " << playerID << " [" << force << "]" << endl;
            player->setForce(force);
            return;
        }
    }
}


int PlayerGroup::getTotalForce(){
    int total = 0;

    for(auto& player : players){
        total += player->getAverageForce();
    }

    total /= players.size();

    return total;
}


std::vector<Player*> PlayerGroup::getPlayers(){
    return players;
}

int PlayerGroup::getSize(){
    return players.size();
}


sf::Vector2f PlayerGroup::getPosition(){
    return players.at(0)->getPosition();
}


void PlayerGroup::setPosition(sf::Vector2f pos){
    int n = players.size();

    for(int i = 0; i < n; i++){
        players.at(i)->setPosition(sf::Vector2f(pos.x - i*PLAYER_SIZE*0.7, pos.y));
    }
}

void PlayerGroup::setLeft(){
    focusMeter.clearAll();
    for(auto& player : players){
       player->setLeft();
    }
    focusMeter.addPlayers(players);
}
